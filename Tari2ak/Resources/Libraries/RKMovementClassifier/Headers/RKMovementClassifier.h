//
//  RKMovementClassifier.h
//  Test
//
//  Created by Rami on 7/4/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RKMovementType.h"

// Called when movement detection has ended
typedef void (^ RKMovementHandlerBlock)(NSArray *probabilities, RKMovementType movement);

@class RDFClassifier;
@class CMMotionManager;

@interface RKMovementClassifier : NSObject

+ (RKMovementClassifier *) sharedInstance;

- (void) detectMovementWithCompletionBlock:(RKMovementHandlerBlock)theHandlerBlock;
- (void) cancelMovementDetection;

@property (nonatomic, assign) BOOL detectionInProgress;

@end
