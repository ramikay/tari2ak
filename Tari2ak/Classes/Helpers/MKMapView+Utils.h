//
//  MKMapView+Utils.h
//  Tari2ak
//
//  Created by Rami Khawandi on 160/6/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKMapView (Utils)

@property (nonatomic, readonly) double zoomLevel;

@end
