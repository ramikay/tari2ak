//
//  NSDate+Sanity.h
//  Tari2ak
//
//  Created by Rami Khawandi on 27/5/14.
//  Copyright (c) 2014 Rami Khawandi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Sanity)

+ (NSDate *) saneNotificationTimeForDate:(NSDate *)aDate showOnlyInMorning:(BOOL)morningOnly;

@end
