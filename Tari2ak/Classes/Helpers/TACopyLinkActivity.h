//
//  TACopyLinkActivity.h
//  Tari2ak
//
//  Created by Rami Khawandi on 16/1/14.
//  Copyright (c) 2014 Rami Khawandi. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const TAActivityTypeCopyLink;

@interface TACopyLinkActivity : UIActivity

- (id) initWithURL:(NSURL *)theURL;

@property (nonatomic, copy) NSURL *url;

@end
