//
//  NSBundle+Helpers.m
//  Tari2ak
//
//  Created by Rami Khawandi on 4/12/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import "NSBundle+Helpers.h"

@implementation NSBundle (Helpers)

//---------------------------------------------
// Implementation
//---------------------------------------------
#pragma mark - Implementation

- (id) loadObjectFromNibNamed:(NSString *)theNibName class:(Class)theClass
{
    return [self loadObjectFromNibNamed:theNibName class:theClass owner:nil options:nil];
}

- (id) loadObjectFromNibNamed:(NSString *)theNibName class:(Class)theClass owner:(id)theOwner options:(NSDictionary *)theOptions
{
    NSArray *_nib = [self loadNibNamed:theNibName owner:theOwner options:theOptions];
    
    for(id _object in _nib)
    {
        if([_object isKindOfClass:theClass])
        {
            return _object;
        }
    }
    
    return nil; // not found
}

@end
