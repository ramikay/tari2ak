//
//  UIView+Utils.h
//  Tari2ak
//
//  Created by Rami Khawandi on 4/9/15.
//  Copyright (c) 2015 Rami Khawandi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Utils)

@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;

- (void) setWidth:(CGFloat)width height:(CGFloat)height;

- (UIImage *) captureSnapshot;

@end
