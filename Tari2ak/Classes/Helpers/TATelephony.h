//
//  TATelephony.h
//  Tari2ak
//
//  Created by Rami Khawandi on 4/12/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TATelephony : NSObject

+ (BOOL) allowsVOIP;
+ (NSString *) carrierDescription;
+ (NSString *) carrierName;
+ (NSString *) isoCountryCode;
+ (NSString *) mobileCountryCode;
+ (NSString *) mobileNetworkCode;

@end
