//
//  NSDate+RelativeTime.h
//  Tari2ak
//
//  Created by Rami Khawandi on 22/4/14.
//  Copyright (c) 2014 Rami Khawandi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (RelativeTime)

+ (NSString*)timeAgoSinceDate:(NSDate *)date;
- (NSString*)timeAgoSinceNow;
- (NSString *)timeAgoSinceDate:(NSDate *)date;

@end
