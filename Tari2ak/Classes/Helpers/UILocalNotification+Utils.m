//
//  UILocalNotification+Utils.m
//  Tari2ak
//
//  Created by Rami Khawandi on 16/1/14.
//  Copyright (c) 2014 Rami Khawandi. All rights reserved.
//

#import "UILocalNotification+Utils.h"

@implementation UILocalNotification (Utils)

+ (void) showNotificationWithMessage:(NSString *)theMessage sound:(BOOL)hasSound
{
    UILocalNotification *_notif = [[UILocalNotification alloc] init];
    _notif.alertBody = theMessage;
    if (hasSound) { _notif.soundName = UILocalNotificationDefaultSoundName; }
    [[UIApplication sharedApplication] presentLocalNotificationNow:_notif];
}

+ (void) scheduleNotificationWithMessage:(NSString *)theMessage sound:(BOOL)hasSound onDate:(NSDate *)thedate
{
    UILocalNotification *_ln = [[UILocalNotification alloc] init];
    _ln.alertBody = theMessage;
    _ln.fireDate = thedate;
    
    if (hasSound)
    {
        _ln.soundName = UILocalNotificationDefaultSoundName;
    }
    
    [[UIApplication sharedApplication] scheduleLocalNotification:_ln];
}

@end
