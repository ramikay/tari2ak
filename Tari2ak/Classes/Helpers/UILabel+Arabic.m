//
//  UILabel+Arabic.m
//  Tari2ak
//
//  Created by Rami Khawandi on 15/12/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import "UILabel+Arabic.h"
#import "ArabicConverter.h"

@implementation UILabel (Arabic)

- (void) setArabicText:(NSString *)aString
{
    self.text = [ArabicConverter convertArabic:aString];
}

@end
