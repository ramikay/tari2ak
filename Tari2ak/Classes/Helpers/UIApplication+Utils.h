//
//  UIApplication+Utils.h
//  Tari2ak
//
//  Created by Rami Khawandi on 9/10/15.
//  Copyright © 2015 Rami Khawandi. All rights reserved.
//


@interface UIApplication (Utils)

- (void) dismissAllAlertViews;

@end
