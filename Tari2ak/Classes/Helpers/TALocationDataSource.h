//
//  TALocationDataSource.h
//  Tari2ak
//
//  Created by Rami Khawandi on 27/4/14.
//  Copyright (c) 2014 Rami Khawandi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

// Fired whenever the user changes location
extern NSString * const TAUserLocationChanged;

@interface TALocationDataSource : NSObject <CLLocationManagerDelegate>

+ (TALocationDataSource *) sharedInstance;

@property (nonatomic, strong) CLLocation *lastUserLocation;

@end
