//
//  UIView+Utils.m
//  Tari2ak
//
//  Created by Rami Khawandi on 4/9/15.
//  Copyright (c) 2015 Rami Khawandi. All rights reserved.
//

#import "UIView+Utils.h"

@implementation UIView (Utils)

- (CGFloat) width
{
    return self.bounds.size.width;
}

- (CGFloat) height
{
    return self.bounds.size.height;
}

- (void) setWidth:(CGFloat)width
{
    self.bounds = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, width, self.bounds.size.height);
}

- (void) setHeight:(CGFloat)height
{
    self.bounds = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, height);
}

- (void) setWidth:(CGFloat)width height:(CGFloat)height
{
    self.bounds = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, width, height);
}

- (UIImage *) captureSnapshot
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, YES, 0);
    [self drawViewHierarchyInRect:self.bounds afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
