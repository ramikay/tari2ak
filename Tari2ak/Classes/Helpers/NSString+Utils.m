//
//  NSString+Utils.m
//  Tari2ak
//
//  Created by Rami Khawandi on 19/5/15.
//  Copyright (c) 2015 Rami Khawandi. All rights reserved.
//

#import "NSString+Utils.h"

@implementation NSString (Utils)

- (NSString *) stringByEscapingQuotes
{
    return [self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

@end
