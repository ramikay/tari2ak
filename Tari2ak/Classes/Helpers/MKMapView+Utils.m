//
//  MKMapView+Utils.m
//  Tari2ak
//
//  Created by Rami Khawandi on 160/6/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import "MKMapView+Utils.h"

#define MERCATOR_RADIUS 85445659.44705395
#define MAX_GOOGLE_LEVELS 20

@implementation MKMapView (Utils)

- (double) zoomLevel
{
    CLLocationDegrees longitudeDelta = self.region.span.longitudeDelta;
    CGFloat mapWidthInPixels = self.bounds.size.width;
    double zoomScale = longitudeDelta * MERCATOR_RADIUS * M_PI / (180.0 * mapWidthInPixels);
    double zoomer = MAX_GOOGLE_LEVELS - log2( zoomScale );
    if ( zoomer < 0 ) zoomer = 0;
    return round(zoomer);
}

@end
