//
//  TACopyLinkActivity.m
//  Tari2ak
//
//  Created by Rami Khawandi on 16/1/14.
//  Copyright (c) 2014 Rami Khawandi. All rights reserved.
//

#import "TACopyLinkActivity.h"

NSString * const TAActivityTypeCopyLink = @"com.ramikay.activity.copylink";

@implementation TACopyLinkActivity

//------------------------------------------
// Init
//------------------------------------------
#pragma mark - Init

- (id) initWithURL:(NSURL *)theURL
{
    self = [super init];
    
    if (self != nil)
    {
        self.url = theURL;
    }
    
    return self;
}

//------------------------------------------
// UIActivity methods
//------------------------------------------
#pragma mark - UIActivity methods

- (NSString *)activityType
{
    return TAActivityTypeCopyLink;
}

- (NSString *)activityTitle
{
    return TALocalizedString(@"Copy Link");
}

- (UIImage *)activityImage
{
    NSString *_imageName = @"copy-link-icon-6.png";
    
    if (IS_IOS_7_OR_NEWER)
    {
        _imageName = @"copy-link-icon.png";
    }
    
    return [UIImage imageNamed:_imageName];
}

- (BOOL)canPerformWithActivityItems:(NSArray *)theActivityItems
{
    return YES;
}

- (void)performActivity
{
    if (self.url)
    {
        UIPasteboard *_pasteboard = [UIPasteboard generalPasteboard];
        _pasteboard.URL = self.url;
    }
    
    [self activityDidFinish:YES];
}

@end
