//
//  CLLocation+Utils.h
//  Tari2ak
//
//  Created by Rami Khawandi on 159/6/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

@interface CLLocation (Utils)

// Returns the speed between this location and another location in meters/second
- (float) speedFromLocation:(CLLocation *)aLocation;

/* Returns the angle (in degrees) between 2 points and the north
 * axis, considering the first point as the origin.
 *
 * Note that the receiving location object must arrive before
 * the passed parameter (aLocation) in time.
 **/
- (double) northAngleWithLocation:(CLLocation *)aLocation;

/*
 * Offsets the latitude/longitude of a point with respective distances (in meters)
 *
 * I) Traveling horizontally:
 * 1. To travel east, longitudeOffset > 0
 * 2. To travel west, longitudeOffset < 0
 *
 * II) Traveling vertically:
 * 1. To travel north, latitudeOffset > 0
 * 2. To travel south, latitudeOffset < 0
 */
- (CLLocation *) locationByOffsettingLongitude:(CLLocationDistance)longitudeOffset
                                      latitude:(CLLocationDistance)latitudeOffset;

@end
