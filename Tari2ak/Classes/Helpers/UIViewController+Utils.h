//
//  UIViewController+Utils.h
//  Tari2ak
//
//  Created by Rami Khawandi on 11/6/14.
//  Copyright (c) 2014 Rami Khawandi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Utils)

- (BOOL) isVisble;

@end
