//
//  TALocationDataSource.m
//  Tari2ak
//
//  Created by Rami Khawandi on 27/4/14.
//  Copyright (c) 2014 Rami Khawandi. All rights reserved.
//

#import "TALocationDataSource.h"
#import "TAAppDelegate.h"

@interface TALocationDataSource ()
{
    CLLocationManager *locationManager;
    CLLocation *lastLocation;
}
- (void) startLocationUpdates;
- (void) stopLocationUpdates;
@end

//------------------------------------------
// Notifications
//------------------------------------------
#pragma mark - Notifications

NSString * const TAUserLocationChanged = @"TAUserLocationChanged";

@implementation TALocationDataSource

//------------------------------------------
// Singleton methods
//------------------------------------------
#pragma mark - Singleton methods

+ (TALocationDataSource *) sharedInstance
{
    static dispatch_once_t pred;
    static TALocationDataSource *_shared = nil;
    
    dispatch_once(&pred, ^{
        _shared = [[TALocationDataSource alloc] init];
    });
    
    return _shared;
}

//------------------------------------------
// Init
//------------------------------------------
#pragma mark - Init

- (id) init
{
    self = [super init];
    
    if (self != nil)
    {
        [self startLocationUpdates];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    }
    
    return self;
}

//------------------------------------------
// Public methods
//------------------------------------------
#pragma mark - Public methods

- (CLLocation *) lastUserLocation
{
    return lastLocation;
}

//------------------------------------------
// Actions
//------------------------------------------
#pragma mark - Actions

- (void) didEnterBackground:(NSNotification *)theNotification
{
    [self stopLocationUpdates];
}

- (void) willEnterForeground:(NSNotification *)theNotification
{
    [self startLocationUpdates];
}

//------------------------------------------
// Private methods
//------------------------------------------
#pragma mark - Private methods

- (void) startLocationUpdates
{
    CLAuthorizationStatus _authorizationStatus = [CLLocationManager authorizationStatus];
    
    if (_authorizationStatus == kCLAuthorizationStatusAuthorized)
    {
        if (!locationManager)
        {
            locationManager = [[CLLocationManager alloc] init];
            locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
        }
        
        locationManager.delegate = self;
        [locationManager startUpdatingLocation];
    }
}

- (void) stopLocationUpdates
{
    [locationManager stopUpdatingLocation];
    locationManager = nil;
    lastLocation = nil;
}

//------------------------------------------
// CLLocationManagerDelegate methods
//------------------------------------------
#pragma mark - CLLocationManagerDelegate methods

- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    locationManager.distanceFilter = 10;
    lastLocation = [locations lastObject];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:TAUserLocationChanged object:lastLocation];
}

@end
