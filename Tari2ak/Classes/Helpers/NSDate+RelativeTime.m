//
//  NSDate+RelativeTime.m
//  Tari2ak
//
//  Created by Rami Khawandi on 22/4/14.
//  Copyright (c) 2014 Rami Khawandi. All rights reserved.
//

#import "NSDate+RelativeTime.h"

@implementation NSDate (RelativeTime)

/**
 *  Takes in a date and returns a string with the most convenient unit of time representing
 *  how far in the past that date is from now.
 *
 *  @param NSDate - Date to be measured from now
 *
 *  @return NSString - Formatted return string
 */
+ (NSString*)timeAgoSinceDate:(NSDate*)date
{
    return [date timeAgoSinceDate:[NSDate date]];
}

/**
 *  Returns a string with the most convenient unit of time representing
 *  how far in the past that date is from now.
 *
 *  @return NSString - Formatted return string
 */
- (NSString*)timeAgoSinceNow
{
    return [self timeAgoSinceDate:[NSDate date]];
}

- (NSString *)timeAgoSinceDate:(NSDate *)date
{

    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSSecondCalendarUnit | NSMinuteCalendarUnit | NSHourCalendarUnit | NSDayCalendarUnit;
    NSDate *earliest = [self earlierDate:date];
    NSDate *latest = (earliest == self) ? date : self;
    NSDateComponents *components = [calendar components:unitFlags fromDate:earliest toDate:latest options:0];
    
    if (components.day >= 11)
    {
        return [NSString stringWithFormat:TALocalizedString(@"%d xx days ago"), components.day];
    }
    else if (components.day >= 3)
    {
        return [NSString stringWithFormat:TALocalizedString(@"%d x days ago"), components.day];
    }
    else if (components.day >= 2)
    {
        return [NSString stringWithFormat:TALocalizedString(@"2 days ago"), components.day];
    }
    else if (components.day >= 1)
    {
        return TALocalizedString(@"1 day ago");
    }
    else if (components.hour >= 11)
    {
        return [NSString stringWithFormat:TALocalizedString(@"%d xx hrs ago"), components.hour];
    }
    else if (components.hour >= 3)
    {
        return [NSString stringWithFormat:TALocalizedString(@"%d x hrs ago"), components.hour];
    }
    else if (components.hour >= 2)
    {
        return [NSString stringWithFormat:TALocalizedString(@"2 hrs ago"), components.hour];
    }
    else if (components.hour >= 1)
    {
        return TALocalizedString(@"1 hr ago");
    }
    else if (components.minute >= 11)
    {
        return [NSString stringWithFormat:TALocalizedString(@"%d xx mins ago"), components.minute];
    }
    else if (components.minute >= 3)
    {
        return [NSString stringWithFormat:TALocalizedString(@"%d x mins ago"), components.minute];
    }
    else if (components.minute >= 2)
    {
        return [NSString stringWithFormat:TALocalizedString(@"2 mins ago"), components.minute];
    }
    else if (components.minute >= 1)
    {
        return TALocalizedString(@"1 min ago");
    }
    else if (components.second >= 11)
    {
        return [NSString stringWithFormat:TALocalizedString(@"%d xx secs ago"), components.second];
    }
    else if (components.second >= 3)
    {
        return [NSString stringWithFormat:TALocalizedString(@"%d x secs ago"), components.second];
    }
    else
    {
        return TALocalizedString(@"Just now");
    }
    
}

@end
