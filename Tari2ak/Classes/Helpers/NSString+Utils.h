//
//  NSString+Utils.h
//  Tari2ak
//
//  Created by Rami Khawandi on 19/5/15.
//  Copyright (c) 2015 Rami Khawandi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utils)

- (NSString *) stringByEscapingQuotes;

@end
