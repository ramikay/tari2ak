//
//  UIImage+Utils.h
//  Tari2ak
//
//  Created by Rami Khawandi on 10/9/15.
//  Copyright (c) 2015 Rami Khawandi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Utils)

- (UIImage *)imageByCroppingToSize:(CGSize)size;

@end
