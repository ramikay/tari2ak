//
//  TAStreet+Geography.h
//  Tari2ak
//
//  Created by Rami Khawandi on 21/5/15.
//  Copyright (c) 2015 Rami Khawandi. All rights reserved.
//

#import "TAStreet.h"

extern NSString * const TAAreaKeySublocality;
extern NSString * const TAAreaKeyLocality;
extern NSString * const TAAreaKeyAdministrative;
extern NSString * const TAAreaKeySubAdministrative;
extern NSString * const TAAreaKeyCountryName;
extern NSString * const TAAreaKeyCountryCode;

@interface TAStreet (Geography)

- (void) areaDetails:(void (^)(NSDictionary *areaDetails, NSError *error))handlerBlock;

@end
