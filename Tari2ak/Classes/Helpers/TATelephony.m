//
//  TATelephony.m
//  Tari2ak
//
//  Created by Rami Khawandi on 4/12/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import "TATelephony.h"
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>


@implementation TATelephony

+ (BOOL) allowsVOIP
{
    CTTelephonyNetworkInfo *_phoneInfo = [[CTTelephonyNetworkInfo alloc] init];
    return [[_phoneInfo subscriberCellularProvider] allowsVOIP];
}

+ (NSString *) carrierDescription
{
    CTTelephonyNetworkInfo *_phoneInfo = [[CTTelephonyNetworkInfo alloc] init];
    return [[_phoneInfo subscriberCellularProvider] description];
}

+ (NSString *) carrierName
{
    CTTelephonyNetworkInfo *_phoneInfo = [[CTTelephonyNetworkInfo alloc] init];
    return [[_phoneInfo subscriberCellularProvider] carrierName];
}

+ (NSString *) isoCountryCode
{
    CTTelephonyNetworkInfo *_phoneInfo = [[CTTelephonyNetworkInfo alloc] init];
    return [[_phoneInfo subscriberCellularProvider] isoCountryCode];
}

+ (NSString *) mobileCountryCode
{
    CTTelephonyNetworkInfo *_phoneInfo = [[CTTelephonyNetworkInfo alloc] init];
    return [[_phoneInfo subscriberCellularProvider] mobileCountryCode];
}

+ (NSString *) mobileNetworkCode
{
    CTTelephonyNetworkInfo *_phoneInfo = [[CTTelephonyNetworkInfo alloc] init];
    return [[_phoneInfo subscriberCellularProvider] mobileNetworkCode];
}

@end
