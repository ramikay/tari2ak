//
//  NSDate+Sanity.m
//  Tari2ak
//
//  Created by Rami Khawandi on 27/5/14.
//  Copyright (c) 2014 Rami Khawandi. All rights reserved.
//

#import "NSDate+Sanity.h"

@implementation NSDate (Sanity)

+ (NSDate *) saneNotificationTimeForDate:(NSDate *)aDate showOnlyInMorning:(BOOL)morningOnly
{
    NSDate *_notificationTime = aDate;
    
    NSCalendar *_gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *_components = [_gregorianCalendar components:(NSHourCalendarUnit | NSWeekdayCalendarUnit) fromDate:_notificationTime];
    NSInteger _nextHour = [_components hour];
    NSInteger _today    = [_components weekday];
    NSInteger _tomorrow = (_today % 7) + 1;
    
    // If it's before 8 AM, schedule it for wake up time
    if (_nextHour < 8)
    {
        NSDateComponents *_newComponents = [_gregorianCalendar components:(NSHourCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSMinuteCalendarUnit) fromDate:_notificationTime];;
        
        // If today is a weekend, schedule it for 10 AM
        if (_today == 1 || _today == 7)
        {
            [_newComponents setHour:10];
        }
        // Otherwise, schedule it for 8 AM
        else
        {
            [_newComponents setHour:8];
        }
        
        [_newComponents setYear:_newComponents.year];
        [_newComponents setMonth:_newComponents.month];
        [_newComponents setDay:_newComponents.day];
        [_newComponents setMinute:0];
        
        _notificationTime = [_gregorianCalendar dateFromComponents:_newComponents];
    }
    
    // If it's after 8 PM, schedule it for the next day
    else if (morningOnly || _nextHour >= 20)
    {
        // Schedule it for tomorrow
        _notificationTime = [_notificationTime dateByAddingTimeInterval:60*60*24];
        
        NSDateComponents *_newComponents = [_gregorianCalendar components:(NSHourCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSMinuteCalendarUnit) fromDate:_notificationTime];;
        
        // If tomorrow is a weekend, schedule it for 10 AM
        if (_tomorrow == 1 || _tomorrow == 7)
        {
            [_newComponents setHour:10];
        }
        // Otherwise, schedule it for 8 AM
        else
        {
            [_newComponents setHour:8];
        }
        
        [_newComponents setYear:_newComponents.year];
        [_newComponents setMonth:_newComponents.month];
        [_newComponents setDay:_newComponents.day];
        [_newComponents setMinute:0];
        
        _notificationTime = [_gregorianCalendar dateFromComponents:_newComponents];
    }
    
    return _notificationTime;
}

@end
