//
//  TAConstants.h
//  Tari2ak
//
//  Created by Rami on 24/5/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#ifndef Tari2ak_TAConstants_h
#define Tari2ak_TAConstants_h

#define SERVER_URL      @"http://api.tari2ak.com"
#define SANDBOX_URL     @"http://sandbox.tari2ak.com/api"
#define REPORTING_KEY   @"DwR@Jxc3U^5M"
#define RETRIEVAL_KEY   @"B7XezTUbHoozpyTB"


// Geolocation constants
#define METERS_PER_MILE 1609.344
#define METERS_PER_DEGREE 111111

#define degreesToRadians( degrees ) ( ( degrees ) / 180.0 * M_PI )
#define radiansToDegrees( radians ) ( ( radians ) * ( 180.0 / M_PI ) )


// User interface
#define NAVIGATION_BAR_BACKGROUND_COLOR         RGB(66,66,66)
#define UPDATE_CYCLE    180         // Number of seconds before the data is auto-refreshed



// Business logic
#define ALLOWED_USES_WITHOUT_LOCATION           45


#define APP_STORE_ID                    @"783043884"

// Google Analytics
#define GOOGLE_ANALYTICS_TRACKING_ID    @"UA-46815005-1"
#define DISPATCH_INTERVAL_WIFI          20
#define DISPATCH_INTERVAL_WWAN          300

// NSUserDefaults Keys and Values
#define INITIAL_LOCALE_KEY              @"Tari2akLocale"
#define LOCALE_ARABIC_LEBANON           @"ar-LB"
#define LOCALE_ARABIC                   @"ar"
#define LOCALE_ENGLISH                  @"en"
#define LOCALE_FRENCH                   @"fr"
#define LOCALE_ARMENIAN                 @"hy"

// Obsolete
//#define INITIAL_VIEW_KEY                @"Tari2akInitialView"
//#define INITIAL_VIEW_MAP                @"Map"
//#define INITIAL_VIEW_LIST               @"List"

#define INITIAL_MAP_TYPE_KEY            @"Tari2akMapType"
#define INITIAL_MAP_MODE_KEY            @"Tari2akMapMode"

#define INITIAL_MAP_CAMERA_LATITUDE     @"Tari2akMapCameraLatitude"
#define INITIAL_MAP_CAMERA_LONGITUDE    @"Tari2akMapCameraLongitude"
#define INITIAL_MAP_CAMERA_HEADING      @"Tari2akMapCameraHeading"
#define INITIAL_MAP_CAMERA_PITCH        @"Tari2akMapCameraPitch"
#define INITIAL_MAP_CAMERA_ALTITUDE     @"Tari2akMapCameraAltitude"

#define DISABLED_LOCATION_KEY           @"Tari2akLocationDisabledCount"

#endif
