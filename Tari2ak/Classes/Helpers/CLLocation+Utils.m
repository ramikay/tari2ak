//
//  CLLocation+Utils.m
//  Tari2ak
//
//  Created by Rami Khawandi on 159/6/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import "CLLocation+Utils.h"
#import "TAConstants.h"
#import <math.h>

@implementation CLLocation (Utils)

- (float) speedFromLocation:(CLLocation *)aLocation;
{
    CLLocationDistance _distance = [self distanceFromLocation:aLocation];
    
    NSTimeInterval _time = [self.timestamp timeIntervalSinceDate:aLocation.timestamp];
    
    return _distance / _time;
}

- (double) northAngleWithLocation:(CLLocation *)aLocation;
{
    CLLocationDegrees _lat1 = self.coordinate.latitude;
	CLLocationDegrees _lat2 = aLocation.coordinate.latitude;
	CLLocationDegrees _lon1 = self.coordinate.longitude;
	CLLocationDegrees _lon2 = aLocation.coordinate.longitude;
    
	if (_lat1 != _lat2 || _lon1 != _lon2)
	{
		if (_lon1 == _lon2)		// User is traveling horizontally
		{
			if (_lat1 > _lat2) return -90.0;
			if (_lat1 < _lat1) return 90.0;
		}
		else if (_lat1 == _lat2)	// User is travling vertically
		{
			if (_lon1 > _lon2) return -180.0;
			if (_lon2 < _lon1) return 180.0;
		}
		else	// %99.99 percent of cases go here
		{
			// Third point needed to construct the right triangle
			CLLocation *_point3 = [[CLLocation alloc] initWithLatitude:_lat1 longitude:_lon2];
            
    		double p1p3 = [self distanceFromLocation:_point3];
    		double p1p2 = [self distanceFromLocation:aLocation];
            
    		double _angle = acos(p1p3/p1p2) * (180/M_PI);
            
    		if (_lon1 > _lon2) _angle = 180 - _angle;
    		if (_lat1 > _lat2) _angle = -1 * _angle;
            
    		return _angle;
		}
	}
	else
	{
		// Points are identical :|
		// User is not on the move :O
		// Oh dear lord how did it come to this? =/
		// WHAT DO WE DO NOOOOOWWWWW? ='(
        
		return 0;
	}
    
    return 0;
}


- (CLLocation *) locationByOffsettingLongitude:(CLLocationDistance)longitudeOffset
                                      latitude:(CLLocationDistance)latitudeOffset
{
    CLLocationDegrees newLongitude;
    CLLocationDegrees newLatitude;
    
    if (latitudeOffset == 0)
    {
        newLatitude = self.coordinate.latitude;
    }
    else
    {
        newLatitude = self.coordinate.latitude + latitudeOffset / METERS_PER_DEGREE;
    }
    
    if (longitudeOffset == 0)
    {
        newLongitude = self.coordinate.longitude;
    }
    else
    {
        newLongitude = self.coordinate.longitude + longitudeOffset / METERS_PER_DEGREE / cos(degreesToRadians(self.coordinate.latitude));
    }
    
    return [[CLLocation alloc] initWithLatitude:newLatitude longitude:newLongitude];
}

@end
