//
//  TAStreet+Geography.m
//  Tari2ak
//
//  Created by Rami Khawandi on 21/5/15.
//  Copyright (c) 2015 Rami Khawandi. All rights reserved.
//

#import "TAStreet+Geography.h"
#import "TALocaleManager.h"
#import <CoreLocation/CoreLocation.h>

NSString * const TAAreaKeySublocality          = @"TAAreaKeySublocality";
NSString * const TAAreaKeyLocality             = @"TAAreaKeyLocality";
NSString * const TAAreaKeyAdministrative       = @"TAAreaKeyAdministrative";
NSString * const TAAreaKeySubAdministrative    = @"TAAreaKeySubAdministrative";
NSString * const TAAreaKeyCountryName          = @"TAAreaKeyCountryName";
NSString * const TAAreaKeyCountryCode          = @"TAAreaKeyCountryCode";

@implementation TAStreet (Geography)

- (void) areaDetails:(void (^)(NSDictionary *, NSError *))handlerBlock
{
    __block NSMutableDictionary *areaDetails = [[NSMutableDictionary alloc] init];
    
    if (handlerBlock && self.nodes.count > 0)
    {
        CLLocation *firstPoint = [[self nodes] objectAtIndex:0];
        
        CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
        [geoCoder reverseGeocodeLocation:firstPoint completionHandler:^(NSArray *placemarks, NSError *error) {
            
            if (error)
            {
                handlerBlock(nil, error);
            }
            else
            {
                if (placemarks.count > 0)
                {
                    CLPlacemark *placemark = [placemarks objectAtIndex:0];
                    
                    if (placemark.locality) { [areaDetails setObject:placemark.locality forKey:TAAreaKeyLocality]; }
                    if (placemark.subLocality) { [areaDetails setObject:placemark.subLocality forKey:TAAreaKeySublocality]; }
                    if (placemark.administrativeArea) { [areaDetails setObject:placemark.administrativeArea forKey:TAAreaKeyAdministrative]; }
                    if (placemark.subAdministrativeArea) { [areaDetails setObject:placemark.subAdministrativeArea forKey:TAAreaKeySubAdministrative]; }
                    if (placemark.country) { [areaDetails setObject:placemark.country forKey:TAAreaKeyCountryName]; }
                    if (placemark.ISOcountryCode) { [areaDetails setObject:placemark.ISOcountryCode forKey:TAAreaKeyCountryCode]; }
                }
                
                handlerBlock(areaDetails, nil);
            }
        }];
    }
    else
    {
        NSError *_error = [NSError errorWithDomain:@"com.tari2ak.error" code:140 userInfo:@{NSLocalizedDescriptionKey:@"Street object has no nodes."}];
        handlerBlock(nil, _error);
    }
}

@end
