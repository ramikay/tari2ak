//
//  UIViewController+Utils.m
//  Tari2ak
//
//  Created by Rami Khawandi on 11/6/14.
//  Copyright (c) 2014 Rami Khawandi. All rights reserved.
//

#import "UIViewController+Utils.h"

@implementation UIViewController (Utils)

- (BOOL) isVisble
{
    return (self.isViewLoaded && self.view.window);
}

@end
