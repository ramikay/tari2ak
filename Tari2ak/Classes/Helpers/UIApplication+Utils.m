//
//  UIApplication+Utils.m
//  Tari2ak
//
//  Created by Rami Khawandi on 9/10/15.
//  Copyright © 2015 Rami Khawandi. All rights reserved.
//

#import "UIApplication+Utils.h"

@implementation UIApplication (Utils)

- (void) dismissAllAlertViews
{
    for (UIWindow* window in self.windows)
    {
        NSArray* subviews = window.subviews;
        if ([subviews count] > 0)
        {
            if ([[subviews objectAtIndex:0] isKindOfClass:[UIAlertView class]])
            {
                [(UIAlertView *)[subviews objectAtIndex:0] dismissWithClickedButtonIndex:[(UIAlertView *)[subviews objectAtIndex:0] cancelButtonIndex] animated:NO];
            }
        }
    }
}

@end
