//
//  UILabel+Arabic.h
//  Tari2ak
//
//  Created by Rami Khawandi on 15/12/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Arabic)

- (void) setArabicText:(NSString *)aString;

@end
