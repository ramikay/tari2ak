//
//  NSBundle+Helpers.h
//  Tari2ak
//
//  Created by Rami Khawandi on 4/12/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Helpers)

/**
 * Loads the nib specified and returns the first instance of theClass in the loaded Objects.
 */
- (id) loadObjectFromNibNamed:(NSString *)theNibName class:(Class)theClass owner:(id)theOwner options:(NSDictionary *)theOptions;


/**
 * Loads the nib specified and returns the first instance of theClass in the loaded Objects.
 */
- (id) loadObjectFromNibNamed:(NSString *)theNibName class:(Class)theClass;

@end
