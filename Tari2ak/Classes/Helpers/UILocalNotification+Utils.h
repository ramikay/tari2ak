//
//  UILocalNotification+Utils.h
//  Tari2ak
//
//  Created by Rami Khawandi on 16/1/14.
//  Copyright (c) 2014 Rami Khawandi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILocalNotification (Utils)

+ (void) showNotificationWithMessage:(NSString *)theMessage sound:(BOOL)hasSound;

+ (void) scheduleNotificationWithMessage:(NSString *)theMessage sound:(BOOL)hasSound onDate:(NSDate *)thedate;

@end
