//
//  TAPolylineRenderer.h
//  TAPolylineRenderer
//
//  Created by Rami Khawandi on 17/7/14.
//  Copyright (c) 2014 Tari2ak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface TAPolylineRenderer : MKOverlayPathRenderer

- (id)initWithPolyline:(MKPolyline *)thePolyline;
- (id)initWithPolyline:(MKPolyline *)thePolyline lineDashPattern:(NSArray *)aLineDashPattern;

- (void) startAnimating;
- (void) stopAnimating;

@property (nonatomic, readonly) BOOL isAnimating;

@property (nonatomic, assign) BOOL hideDashesWhenStopped;

@property (nonatomic, strong) MKPolyline *polyline;

// Set the moving speed in meters per second
@property (nonatomic, assign) CGFloat speed;

@end
