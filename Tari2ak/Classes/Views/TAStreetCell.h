//
//  TAStreetCell.h
//  Tari2ak
//
//  Created by Rami Khawandi on 182/7/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TAStreetCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *streetName;
@property (nonatomic, strong) IBOutlet UILabel *averageSpeed;
@property (nonatomic, strong) IBOutlet UILabel *lastUpdated;
@property (nonatomic, strong) IBOutlet UILabel *proximity;

@property (nonatomic, strong) IBOutlet UIView *trafficIndicator;
@property (nonatomic, strong) IBOutlet UIImageView *speedIndicator;
@property (nonatomic, strong) IBOutlet UIImageView *timeIndicator;
@property (nonatomic, strong) IBOutlet UIImageView *proximityIndicator;

@end
