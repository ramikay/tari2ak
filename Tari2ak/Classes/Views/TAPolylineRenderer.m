//
//  TAPolylineRenderer.m
//  TAPolylineRenderer
//
//  Created by Rami Khawandi on 17/7/14.
//  Copyright (c) 2014 Tari2ak. All rights reserved.
//

#import "TAPolylineRenderer.h"

#define SPEED_MULTIPLIER        6   // Coefficient multiplied by speed assigned to the speed @property
#define DEFAULT_FRAME_INTERVAL  6   //
#define DEFAULT_REFRESH_RATE    60.0f // FPS

@interface TAPolylineRenderer ()
{
    MKPolyline *polyline;
    MKZoomScale lastZoomScale;
    
    CGMutablePathRef mutablePath;
    NSArray *lineDashPatternObject;
    
    BOOL animating;
    NSObject *animationLock;
    
    CGFloat *lineDashPatternArray;
    NSUInteger lineDashCount;
    
    CGFloat lineDashPhase;
    CGFloat lineDashSum;
    NSUInteger phaseInterval;
    NSUInteger phaseCounter;
    
    CADisplayLink *displayLink;
    
    BOOL shouldRecalculateDashes;
    BOOL isCalculating;
}
- (void) calculateDashesWithZoomScale:(MKZoomScale)zoomScale;
- (CGFloat) drawingInterval;
@end

@implementation TAPolylineRenderer

//-----------------------------------
// Init & dealloc
//-----------------------------------
#pragma mark - Init & dealloc

- (id)initWithPolyline:(MKPolyline *)thePolyline lineDashPattern:(NSArray *)aLineDashPattern
{
    self = [super initWithOverlay:thePolyline];
    
    if (self)
    {
        self.polyline = thePolyline;
        
        mutablePath = CGPathCreateMutable();
        animating = NO;
        
        animationLock = [[NSObject alloc] init];
        
        [self createPath];
        
        self.lineDashPattern = aLineDashPattern;
    }
    return self;
}

- (id)initWithPolyline:(MKPolyline *)thePolyline
{
    self = [super initWithOverlay:thePolyline];
    
    if (self)
    {
        self.polyline = thePolyline;
        
        mutablePath = CGPathCreateMutable();
        animating = NO;
        
        animationLock = [[NSObject alloc] init];
        
        [self createPath];
    }
    
    return self;
}

- (void) dealloc
{
    [self stopAnimating];
    
    if (lineDashPatternArray)
    {
        free(lineDashPatternArray);
    }
}

//-----------------------------------
// MKOverlayPathRenderer Overrides
//-----------------------------------
#pragma mark - MKOverlayPathRenderer Overrides

- (void) createPath
{
    BOOL pathIsEmpty = YES;
    
    for (int i = 0; i < self.polyline.pointCount; i++)
    {
        CGPoint point = [self pointForMapPoint:self.polyline.points[i]];
        
        if (pathIsEmpty)
        {
            CGPathMoveToPoint(mutablePath, nil, point.x, point.y);
            pathIsEmpty = NO;
        } else
        {
            CGPathAddLineToPoint(mutablePath, nil, point.x, point.y);
        }
    }
    self.path = mutablePath;
}

//-----------------------------------
// MKOverlayRenderer Overrides
//-----------------------------------
#pragma mark - MKOverlayRenderer Overrides

- (BOOL)canDrawMapRect:(MKMapRect)mapRect zoomScale:(MKZoomScale)zoomScale
{
    return !isCalculating;
}

- (void)drawMapRect:(MKMapRect)mapRect zoomScale:(MKZoomScale)zoomScale inContext:(CGContextRef)context
{
    if (lastZoomScale != zoomScale)
    {
        shouldRecalculateDashes = YES;
    }
    
    if (shouldRecalculateDashes)
    {
        [self calculateDashesWithZoomScale:zoomScale];
    }
    
    lastZoomScale = zoomScale;
    
    @synchronized(animationLock)
    {
        CGContextAddPath(context, mutablePath);
        
        CGContextSetStrokeColorWithColor(context, self.strokeColor.CGColor);
        
        CGFloat _lineWidth = (!self.lineWidth) ? MKRoadWidthAtZoomScale(zoomScale) : self.lineWidth / zoomScale;
        CGContextSetLineWidth(context, _lineWidth);
        CGContextSetLineCap(context, self.lineCap);
        CGContextSetLineJoin(context, self.lineJoin);
        CGContextSetMiterLimit(context, self.miterLimit);
        
        if (animating || !self.hideDashesWhenStopped)
        {
            CGFloat _phase = 0 - ((int)lineDashPhase)%(int)(lineDashSum);
            
            BOOL _shouldIncrementDashPhase = ((++phaseCounter%(int)(phaseInterval)) == 0);
            if (_shouldIncrementDashPhase)
            {
                lineDashPhase = lineDashPhase + SPEED_MULTIPLIER/(displayLink.frameInterval*zoomScale);
            }
            CGContextSetLineDash(context, _phase, lineDashPatternArray, lineDashCount);
        }
        
        [self applyStrokePropertiesToContext:context atZoomScale:zoomScale];
        
        CGContextStrokePath(context);
    }
}

//-----------------------------------
// Private methods
//-----------------------------------
#pragma mark - Private methods

- (void) calculateDashesWithZoomScale:(MKZoomScale)zoomScale
{
    isCalculating = YES;
    
    @synchronized(animationLock)
    {
        
        lineDashCount = lineDashPatternObject.count;
        lineDashSum = 0;
        
        if (lineDashPatternArray)
        {
            free(lineDashPatternArray);
        }
        
        lineDashPatternArray = (CGFloat *)malloc(sizeof(CGFloat) * lineDashPatternObject.count);
        
        for (int _i = 0; _i < lineDashPatternObject.count; _i++)
        {
            float _length = [[lineDashPatternObject objectAtIndex:_i] floatValue] / zoomScale;
            lineDashSum += _length;
            lineDashPatternArray[_i] = _length;
        }
        
        if (displayLink)
        {
            if (self.speed == 0.0)
            {
                phaseInterval = NSIntegerMax;
            }
            else
            {
                // Assume a fixed duration of one over 60 fps
                CGFloat _duration = [self drawingInterval];
                
                CLLocationDistance _metersPerPoint = MKMapPointsPerMeterAtLatitude(self.polyline.coordinate.latitude)/(displayLink.frameInterval * _duration);
                CGFloat interval = ceil(_metersPerPoint/self.speed);
                phaseInterval = (interval > 0) ? interval : NSIntegerMax;
            }
        }
        
        shouldRecalculateDashes = NO;
        
        isCalculating = NO;
    }
}

- (void) maybeNeedsDisplay
{
    [self setNeedsDisplayInMapRect:polyline.boundingMapRect];
}

- (CGFloat) drawingInterval
{
    if (displayLink)
    {
        return MAX(1/DEFAULT_REFRESH_RATE,displayLink.duration);
    }
    else
    {
        return 1/DEFAULT_REFRESH_RATE;
    }
}

//-----------------------------------
// Public methods
//-----------------------------------
#pragma mark - Public methods

- (void) setPolyline:(MKPolyline *)thePolyline
{
    BOOL _wasAnimating = self.isAnimating;
    
    [self stopAnimating];
    
    if (mutablePath)
    {
        CGPathRelease(mutablePath);
    }
    
    polyline = thePolyline;
    
    [self invalidatePath];
    
    if (_wasAnimating)
    {
        [self startAnimating];
    }
}

- (MKPolyline *)polyline
{
    return polyline;
}

- (void) setSpeed:(CGFloat)speed
{
    _speed = speed;
    
    shouldRecalculateDashes = YES;
}

- (void) setLineDashPattern:(NSArray *)aLineDashPattern
{
    lineDashPatternObject = [aLineDashPattern copy];
    
    shouldRecalculateDashes = YES;
}

- (void) startAnimating
{
    NSAssert(lineDashPatternObject.count > 1, @"lineDashProperty must be set before startAnimating is called");
    
    shouldRecalculateDashes = YES;
    
    if (animating)
    {
        [self stopAnimating];
    }
    
    animating = YES;
    if (!displayLink)
    {
        displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(maybeNeedsDisplay)];
        displayLink.frameInterval = DEFAULT_FRAME_INTERVAL;
    }
    
    phaseCounter = 0;
    
    [displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
}

- (void) stopAnimating
{
    animating = NO;
    
    if (displayLink)
    {
        [displayLink removeFromRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
        [displayLink invalidate];
        displayLink = nil;
        animating = NO;
        
        // One more time to fix any frozen frames
        [self maybeNeedsDisplay];
    }
}

@end
