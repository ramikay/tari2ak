//
//  TAStreetCell.m
//  Tari2ak
//
//  Created by Rami Khawandi on 182/7/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import "TAStreetCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation TAStreetCell

@synthesize streetName, averageSpeed, lastUpdated, proximity, trafficIndicator, speedIndicator, timeIndicator, proximityIndicator;

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    self.trafficIndicator.layer.cornerRadius = self.trafficIndicator.frame.size.width/2;
}

@end
