//
//  TAReport.m
//  Tari2ak
//
//  Created by Rami on 7/5/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import "TAReport.h"

@interface TAReport ()
{
    CLLocationCoordinate2D coordinate;
    NSDate *timestamp;
    float speed;
    double angle;
    BOOL inBackground;
}

@end

@implementation TAReport

- (instancetype) initWithCoordinate:(CLLocationCoordinate2D)theCoordinate
                          timestamp:(NSDate *)aTimestamp
                              speed:(float)theSpeed
                         background:(BOOL)isInBackground
                         northAngle:(double)theAngle
{
    self = [super init];
    
    if (self != nil)
    {
        coordinate = theCoordinate;
        inBackground = isInBackground;
        timestamp = aTimestamp;
        
        speed = theSpeed;
        angle = theAngle;
    }
    
    return self;
}

- (CLLocationCoordinate2D) coordinate
{
    return coordinate;
}

- (float) speed
{
    return speed;
}

- (double) northAngle
{
    return angle;
}

- (NSDate *) timestamp
{
    return timestamp;
}

- (BOOL) isInBackground
{
    return inBackground;
}

@end
