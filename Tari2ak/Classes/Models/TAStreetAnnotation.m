//
//  TAStreetAnnotation.m
//  Tari2ak
//
//  Created by Rami Khawandi on 15/5/14.
//  Copyright (c) 2014 Rami Khawandi. All rights reserved.
//

#import "TAStreetAnnotation.h"

@interface TAStreetAnnotation ()
{
    CLLocationCoordinate2D point;
    UIColor *color;
    NSString *streetName;
    NSString *streetStatus;
}
@end

@implementation TAStreetAnnotation

- (id) initWithCoordinate:(CLLocationCoordinate2D)theCoordinate
                    title:(NSString *)theTitle
                 subtitle:(NSString *)theSubtitle
                    color:(UIColor *)theColor
{
    self = [super init];
    
    if (self != nil)
    {
        CLLocationCoordinate2D _point = CLLocationCoordinate2DMake(theCoordinate.latitude, theCoordinate.longitude);
        
        [self setCoordinate:_point];
        streetName = theTitle;
        streetStatus = theSubtitle;
        color = theColor;
    }
    
    return self;
}

- (CLLocationCoordinate2D) coordinate
{
    return point;
}

-(void) setCoordinate:(CLLocationCoordinate2D)newCoordinate
{
    point = newCoordinate;
}

- (NSString *) title
{
    return streetName;
}

- (NSString *) subtitle
{
    return streetStatus;
}

- (UIColor *) color
{
    return color;
}

@end
