//
//  TAStreet.m
//  Tari2ak
//
//  Created by Rami on 24/5/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import "TAStreet.h"
#import "TALocationDataSource.h"
#import "TALocaleManager.h"
#import "TAConstants.h"
#import <CoreLocation/CoreLocation.h>

@interface TAStreet ()
{
    NSString *identifier;
    NSDictionary *names;
    float speed;
    NSArray *nodes;
    CLLocation *midPoint;
    NSString *type;
    TATrafficCondition condition;
    NSDate *lastModificationDate;
    double distanceFromUser;
    CLLocationDistance totalDistance;
    __weak TACountry *country;
    NSMutableArray *areas;
    BOOL isLocked;
}
@end

@implementation TAStreet

//------------------------------------------
// Init & dealloc
//------------------------------------------
#pragma mark - Init & dealloc

- (id) initWithIdentifier:(NSString *)theIdentifier
                    names:(NSDictionary *)theNames
                    speed:(float)theSpeed
                    nodes:(NSArray *)theNodes
                 midPoint:(CLLocation *)theMiddlePoint
                     type:(NSString *)theType
                condition:(TATrafficCondition)theCondition
             modifiedDate:(NSDate *)theModificationDate
{
    self = [super init];
    
    if (self != nil)
    {
        identifier = theIdentifier;
        names = theNames;
        speed = theSpeed;
        nodes = theNodes;
        midPoint = theMiddlePoint;
        type = theType;
        condition = theCondition;
        lastModificationDate = theModificationDate;
        distanceFromUser = -1;
        areas = [[NSMutableArray alloc] init];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLocationDidChange:) name:TAUserLocationChanged object:nil];
    }
    
    return self;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//------------------------------------------
// Properties
//------------------------------------------
#pragma mark - Properties

- (NSString *) identifier
{
    return identifier;
}

- (NSDictionary *) names
{
    return names;
}

- (NSString *) arabicName
{
    return [names objectForKey:@"ar"];
}

- (NSString *) englishName
{
    return [names objectForKey:@"en"];
}

- (NSString *) frenchName
{
    return [names objectForKey:@"fr"];
}

- (NSString *) localizedName
{
    TALocaleManager *localeManager = [TALocaleManager sharedInstance];
    
    NSString *_lang = localeManager.languageID;
    NSString *_name = [self.names objectForKey:localeManager.languageID];
    
    NSArray *_values    = [self.names allValues];
    NSArray *_keys      = [self.names allKeys];
    
    // Try to find an English name first
    if (_name.length == 0)
    {
        _name = [self.names objectForKey:LOCALE_ENGLISH];
        _lang = LOCALE_ENGLISH;
    }
    
    // Search for whatever name is available
    int _i = 0;
    while (_name.length == 0)
    {
        if (_i > ([self.names count] -1))
        {
            break;
        }
        else
        {
            _name = [_values objectAtIndex:_i];
            _lang = [_keys objectAtIndex:_i];
            _i++;
        }
    }
    
    return _name;
}

- (NSArray *) areas
{
    return [NSArray arrayWithArray:areas];
}

- (TACountry *) country
{
    return country;
}

- (float) speed
{
    return speed;
}

- (NSArray *) nodes
{
    return nodes;
}

- (CLLocation *) midPoint
{
    return midPoint;
}

- (NSString *) type
{
    return type;
}

- (TATrafficCondition) condition
{
    return condition;
}

- (NSDate *) lastModificationDate
{
    return lastModificationDate;
}

- (double) distanceFromUser
{
    if (distanceFromUser < 0)
    {
        TALocationDataSource *_dataSource = [TALocationDataSource sharedInstance];
        
        CLLocation *_userLocation = _dataSource.lastUserLocation;
    
        if (_userLocation)
        {
            CLLocationDistance _minDistance = NSIntegerMax;
            
            for (CLLocation *_node in nodes)
            {
                CLLocationDistance _distance = [_node distanceFromLocation:_userLocation];
                
                if (_distance < _minDistance)
                {
                    _minDistance = _distance;
                }
            }
            
            distanceFromUser = _minDistance;
        }
    }
    
    return distanceFromUser;
}

- (BOOL) isLocked
{
    return isLocked;
}

//------------------------------------------
// Overrides
//------------------------------------------
#pragma mark - Overrides

- (BOOL) isEqual:(id)anObject
{
    if ([anObject isKindOfClass:[TAStreet class]])
    {
        TAStreet *_streetObject = (TAStreet *) anObject;
        return [self.identifier isEqualToString:_streetObject.identifier];
    }
    else
    {
        return NO;
    }
}

//------------------------------------------
// Public methods
//------------------------------------------
#pragma mark - Public methods

- (void) lockObject
{
    isLocked = YES;
}

- (void) addArea:(TAArea *)anArea
{
    NSAssert(!isLocked, @"Object is locked and can no longer be mutated");
    
    if (![areas containsObject:anArea])
    {
        [areas addObject:anArea];
    }
}

- (void) removeArea:(TAArea *)anArea
{
    NSAssert(!isLocked, @"Object is locked and can no longer be mutated");
    
    if (![areas containsObject:anArea])
    {
        [areas removeObject:anArea];
    }
}

- (void) setCountry:(TACountry *)theCountry
{
    NSAssert(!isLocked, @"Object is locked and can no longer be mutated");
    
    country = theCountry;
}

//------------------------------------------
// NSNotification methods
//------------------------------------------
#pragma mark - NSNotification methods

- (void) userLocationDidChange:(NSNotification *)theNotification
{
    distanceFromUser = -1;
}

@end
