//
//  TACountry.h
//  Tari2ak
//
//  Created by Rami Khawandi on 21/5/15.
//  Copyright (c) 2015 Rami Khawandi. All rights reserved.
//

#import "TAArea.h"

@interface TACountry : TAArea

- (instancetype) initWithName:(NSString *)theName
                  countryCode:(NSString *)theCountryCode;

@property (nonatomic, readonly) NSString *countryCode;

@end
