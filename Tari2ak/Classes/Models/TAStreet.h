//
//  TAStreet.h
//  Tari2ak
//
//  Created by Rami on 24/5/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CLLocation.h>

@class TAArea, TACountry;

typedef enum
{
    TATrafficConditionUnknown = 0,
    TATrafficConditionHeavy = 1,
    TATrafficConditionMedium = 2,
    TATrafficConditionLow = 3,
    TATrafficConditionVeryHeavy = 4
} TATrafficCondition;

/* An model object to encapsulate data about an actual street */

@interface TAStreet : NSObject

- (id) initWithIdentifier:(NSString *)theIdentifier
                    names:(NSDictionary *)theNames
                    speed:(float)theSpeed
                    nodes:(NSArray *)theNodes
                 midPoint:(CLLocation *)theMiddlePoint
                     type:(NSString *)theType
                condition:(TATrafficCondition)theCondition
             modifiedDate:(NSDate *)theModificationDate;

// Tari'ak Street ID
@property (nonatomic, readonly) NSString *identifier;

@property (nonatomic, readonly) float speed;

@property (nonatomic, readonly) NSString *type;

@property (nonatomic, readonly) TATrafficCondition condition;

@property (nonatomic, readonly) NSDictionary *names;

/**
 * Convenience methods for retrieving localized names.
 * These properties might return nil.
 */
@property (nonatomic, readonly) NSString *arabicName;
@property (nonatomic, readonly) NSString *englishName;
@property (nonatomic, readonly) NSString *frenchName;

@property (nonatomic, readonly) NSString *localizedName;

// CLLocation object indicating the middle of the street
@property (nonatomic, readonly) CLLocation *midPoint;

// An array of CLLocation objects which form the polyline of the street
@property (nonatomic, readonly) NSArray *nodes;

// An array of TAArea objects in which this streets falls within
@property (nonatomic, readonly) NSArray *areas;

@property (nonatomic, readonly) TACountry *country;

// Indicates the last date in which this street's speed was updated
@property (nonatomic, readonly) NSDate *lastModificationDate;

// Returns the distance (in meters) from the last reported user location (or -1 if no user location is found)
@property (nonatomic, readonly) double distanceFromUser;


// Allowed before lockObject is called;
- (void) addArea:(TAArea *)anArea;
- (void) removeArea:(TAArea *)anArea;
- (void) setCountry:(TACountry *)theCountry;

// Disables mutation. Usually called at the end of parsing
- (void) lockObject;

@property (nonatomic, readonly) BOOL isLocked;

@end
