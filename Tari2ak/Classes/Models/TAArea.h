//
//  TAArea.h
//  Tari2ak
//
//  Created by Rami Khawandi on 21/5/15.
//  Copyright (c) 2015 Rami Khawandi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class TACountry;
@class TAStreet;

typedef enum
{
    TAAreaTypeUnknown = -1,
    TAAreaTypeCountry = 0,
    TAAreaTypeAdministrativeArea = 1,
    TAAreaTypeSubAdministrativeArea = 2,
    TAAreaTypeLocality = 3,
    TAAreaTypeSublocality = 4
} TAAreaType;

@interface TAArea : NSObject

- (instancetype) initWithName:(NSString *)theName
                         type:(TAAreaType)theType
                      country:(TACountry *)theCountry;

@property (nonatomic, readonly) NSString *name;

@property (nonatomic, readonly) TAAreaType type;

// TACountry object to which this area falls within
@property (nonatomic, readonly) TACountry *country;

// Array of TAArea objects that are a subset of this area
@property (nonatomic, readonly) NSArray *subareas;

// Array of TAStreet objects that fall within this area
@property (nonatomic, readonly) NSArray *streets;

// Returns the distance (in meters) from the user location during parsing (or -1 if no user location is found)
@property (nonatomic, readonly) CLLocationDistance distanceFromUser;

// Allowed before lockObject is called;
- (void) addSubArea:(TAArea *)anArea;
- (void) removeSubArea:(TAArea *)anArea;

// Allowed before lockObject is called
- (void) addStreet:(TAStreet *)aStreet;
- (void) removeStreet:(TAStreet *)aStreet;


// Disables mutation. Usually called at the end of parsing
- (void) lockObject;

@property (nonatomic, readonly) BOOL isLocked;

@end
