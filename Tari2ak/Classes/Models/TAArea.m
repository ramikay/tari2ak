//
//  TAArea.m
//  Tari2ak
//
//  Created by Rami Khawandi on 21/5/15.
//  Copyright (c) 2015 Rami Khawandi. All rights reserved.
//

#import "TAArea.h"
#import "TAStreet.h"
#import "TACountry.h"

@interface TAArea ()
{
    NSString *name;
    TAAreaType type;
    __weak TACountry *country;
    
    NSMutableArray *subareas;
    NSMutableArray *streets;
    
    CLLocationDistance distanceFromUser;
    
    BOOL isLocked;
}
@end

@implementation TAArea

//------------------------------------------
// Init & dealloc
//------------------------------------------
#pragma mark - Init & dealloc

- (instancetype) initWithName:(NSString *)theName
                         type:(TAAreaType)theType
                      country:(TACountry *)theCountry
{
    self = [super init];
    
    if (self)
    {
        name = theName;
        type = theType;
        country = theCountry;
        
        subareas = [[NSMutableArray alloc] init];
        streets = [[NSMutableArray alloc] init];
        distanceFromUser = -1;
    }
    
    return self;
}

//------------------------------------------
// Properties
//------------------------------------------
#pragma mark - Properties

- (NSString *) name
{
    return name;
}

- (TAAreaType) type
{
    return type;
}

- (TACountry *) country
{
    return country;
}

- (NSArray *) subareas
{
    return [NSArray arrayWithArray:subareas];
}

- (NSArray *) streets
{
    return [NSArray arrayWithArray:streets];
}

- (CLLocationDistance) distanceFromUser
{
    return distanceFromUser;
}

- (BOOL) isLocked
{
    return isLocked;
}

//------------------------------------------
// Public methods
//------------------------------------------
#pragma mark - Public methods

- (void) lockObject
{
    isLocked = YES;
}

- (void) addStreet:(TAStreet *)aStreet
{
    NSAssert(!isLocked, @"Object is locked and can no longer be mutated");
    
    if (![streets containsObject:aStreet])
    {
        [streets addObject:aStreet];

        distanceFromUser = MIN(aStreet.distanceFromUser,distanceFromUser);
    }
}

- (void) removeStreet:(TAStreet *)aStreet
{
    NSAssert(!isLocked, @"Object is locked and can no longer be mutated");
    
    if (![streets containsObject:aStreet])
    {
        [streets removeObject:aStreet];
    }
}

- (void) addSubArea:(TAArea *)anArea
{
    NSAssert(!isLocked, @"Object is locked and can no longer be mutated");
    
    if (![subareas containsObject:anArea])
    {
        [subareas addObject:anArea];
    }
}

- (void) removeSubArea:(TAArea *)anArea
{
    NSAssert(!isLocked, @"Object is locked and can no longer be mutated");
    
    if (![subareas containsObject:anArea])
    {
        [subareas removeObject:anArea];
    }
}

//------------------------------------------
// Overrides
//------------------------------------------
#pragma mark - Overrides

- (BOOL) isEqual:(id)anObject
{
    if ([anObject isKindOfClass:[TAArea class]])
    {
        TAArea *_areaObject = (TAArea *) anObject;
        return (self.country == _areaObject.country && [self.name isEqualToString:_areaObject.name]);
    }
    else
    {
        return NO;
    }
}

@end
