//
//  TAReport.h
//  Tari2ak
//
//  Created by Rami on 7/5/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CLLocation.h>

@interface TAReport : NSObject

- (instancetype) initWithCoordinate:(CLLocationCoordinate2D)theCoordinate
                          timestamp:(NSDate *)aTimestamp
                              speed:(float)theSpeed
                         background:(BOOL)isInBackground
                         northAngle:(double)theAngle;

@property (readonly, nonatomic) CLLocationCoordinate2D coordinate;

/** IMPORTANT
 *  When reading the speed, use this property and not the
 *  speed property which is read from the location property. */
@property (readonly, nonatomic) float speed;

@property (readonly, nonatomic) double northAngle;

@property (readonly, nonatomic) NSDate * timestamp;

@property (readonly, nonatomic) BOOL isInBackground;

@end
