//
//  TACountry.m
//  Tari2ak
//
//  Created by Rami Khawandi on 21/5/15.
//  Copyright (c) 2015 Rami Khawandi. All rights reserved.
//

#import "TACountry.h"

@interface TACountry ()
{
    NSString *countryCode;
}

@end

@implementation TACountry

//------------------------------------------
// Public methods
//------------------------------------------
#pragma mark - Public methods

- (instancetype) initWithName:(NSString *)theName
                  countryCode:(NSString *)theCountryCode
{
    self = [super initWithName:theName
                          type:TAAreaTypeCountry
                       country:nil];
    
    if (self)
    {
        countryCode = theCountryCode;
    }
    return self;
}

- (NSString *) countryCode
{
    return countryCode;
}

//------------------------------------------
// Overrides
//------------------------------------------
#pragma mark - Overrides

- (BOOL) isEqual:(id)anObject
{
    if ([anObject isKindOfClass:[TACountry class]])
    {
        TACountry *_countryObject = (TACountry *) anObject;
        return [self.countryCode isEqualToString:_countryObject.countryCode];
    }
    else
    {
        return NO;
    }
}

@end
