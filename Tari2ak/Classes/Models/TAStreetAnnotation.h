//
//  TAStreetAnnotation.h
//  Tari2ak
//
//  Created by Rami Khawandi on 15/5/14.
//  Copyright (c) 2014 Rami Khawandi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MKAnnotation.h>

@interface TAStreetAnnotation : NSObject  <MKAnnotation>

- (id) initWithCoordinate:(CLLocationCoordinate2D)theCoordinate
                    title:(NSString *)theTitle
                 subtitle:(NSString *)theSubtitle
                    color:(UIColor *)theColor;

- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate;

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly) UIColor *color;

@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly, copy) NSString *subtitle;


@end
