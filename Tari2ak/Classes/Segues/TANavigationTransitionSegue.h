//
//  TANavigationTransitionSegue.h
//  Tari2ak
//
//  Created by Rami Khawandi on 9/12/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    TANavigationPush,
    TANavigationPop
} TANavigationAction;

/**
 * This segue class is meant to be used only by UINavigationControllers
 * for custom transitions when pushing/popping UIViewControllers.
 */
@interface TANavigationTransitionSegue : UIStoryboardSegue

@property (nonatomic, assign) TANavigationAction action;
@property (nonatomic, strong) NSString *transitionType;
@property (nonatomic, strong) NSString *transitionSubtype;
@property (nonatomic, strong) CAMediaTimingFunction *timingFunction;
@property (nonatomic, assign) float duration;

@end
