//
//  TANavigationTransitionSegue.m
//  Tari2ak
//
//  Created by Rami Khawandi on 9/12/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import "TANavigationTransitionSegue.h"
#import <QuartzCore/QuartzCore.h>

@implementation TANavigationTransitionSegue

@synthesize transitionType, transitionSubtype, timingFunction, duration, action;

- (id) initWithIdentifier:(NSString *)theIdentifier source:(UIViewController *)theSource destination:(UIViewController *)theDestination
{
    self = [super initWithIdentifier:theIdentifier source:theSource destination:theDestination];
    
    if (self != nil)
    {
        self.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
    }
    
    return self;
}

- (void) perform
{
    if ([self.sourceViewController isKindOfClass:[UINavigationController class]])
    {
        CATransition *_transition = [CATransition animation];
        _transition.duration = self.duration;
        _transition.timingFunction = self.timingFunction;
        _transition.type = self.transitionType;
        _transition.subtype = self.transitionSubtype;
        
        UINavigationController *_viewController = (UINavigationController *) self.sourceViewController;
        [_viewController.view.layer addAnimation:_transition forKey:kCATransition];
        
        if (action == TANavigationPop)
        {
            [_viewController popViewControllerAnimated:NO];
        }
        else
        {
            [_viewController pushViewController:self.destinationViewController animated:NO];
        }
    }
}

@end
