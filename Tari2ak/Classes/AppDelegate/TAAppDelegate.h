//
//  TAAppDelegate.h
//  Tari2ak
//
//  Created by Rami on 22/3/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "AppiraterDelegate.h"

// Fired when the status of location services changes
// Use the locationServicesEnabled property to determine the value
extern NSString * const TALocationServicesStatusChanged;

// Fired when the status of the app's location authorization changes
// Use the authorizationStatus property to determine the value
extern NSString * const TALocationAuthorizationStatusChanged;

// Fired when the status of the app's location status changes
// Use the locationEnabled property to determine the value
extern NSString * const TALocationStatusChanged;

@interface TAAppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate, AppiraterDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, nonatomic) BOOL locationServicesEnabled;
@property (readonly, nonatomic) CLAuthorizationStatus authorizationStatus;
// Returns yes when authorizationStatus == kCLAuthorizationStatusDenied || authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse
@property (readonly, nonatomic) BOOL deniedFullLocationAccess;

@property (readonly, nonatomic) BOOL backgroundRefreshRequired;
@property (readonly, nonatomic) UIBackgroundRefreshStatus backgroundRefreshStatus;

// Combines the result of authorizationStatus, locationServicesEnabled and
// backgroundRefreshStatus to determine whether location is enabled, regardless of why
@property (readonly, nonatomic) BOOL locationEnabled;

@property (readonly, nonatomic) CLLocation *lastAccurateLocation;

@end
