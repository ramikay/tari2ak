//
//  TAAppDelegate.m
//  Tari2ak
//
//  Created by Rami on 22/3/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import "TAAppDelegate.h"
#import "RKMovementClassifier.h"
#import "TADataReporter.h"
#import "AFHTTPRequestOperation.h"
#import "CLLocation+Utils.h"
#import "TAConstants.h"
#import "TADataRetriever.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "UILocalNotification+Utils.h"
#import "Reachability.h"
#import "NSDate+Sanity.h"
#import "Appirater.h"
#import <CoreMotion/CMMotionActivityManager.h>

#import "NSString+AESCrypt.h"

#define LOW_PASS_FILTER 0.8

//------------------------------------------
// Private interface
//------------------------------------------
#pragma mark - Private interface

@interface TAAppDelegate ()
{
@private
    TADataRetriever *dataRetriever;
    
    CLLocationManager *standardLocationManager;
    CLLocationManager *significantLocationManager;
    
    CLLocation *lastAccurateLocation;
    CLLocation *beforeLastAccurateLocation;
    
    BOOL backgroundRefreshRequired;
    UIBackgroundRefreshStatus backgroundRefreshStatus;
    CLAuthorizationStatus authorizationStatus;
    BOOL locationServicesEnabled;
    BOOL locationEnabled;
    
    id tracker;
    
    dispatch_queue_t backgroundQueue;
    
    UIBackgroundTaskIdentifier bgTask;
}

- (void) scheduleReminderNotifications;
- (BOOL) isInBackground;
- (void) updateLocationServicesAndAuthorization;

- (void) startAccurateLocationUpdates;
- (void) stopAccurateLocationUpdates;
- (void) pauseAccurateLocationUpdatesForDuration:(NSTimeInterval)theDuration;

- (void) sendTestReport;
- (void) maybeReportToServer;
- (void) performActivityCheck;
- (void) dispatch;

- (void) beginBackgroundTask;
- (void) endBackgroundTask;

- (void) initializeAnalyticsTracking;

@end

//------------------------------------------
// Notifications
//------------------------------------------
#pragma mark - Notifications

NSString * const TALocationServicesStatusChanged            = @"TALocationServicesStatusChanged";
NSString * const TALocationAuthorizationStatusChanged       = @"TALocationAuthorizationStatusChanged";
NSString * const TALocationStatusChanged                    = @"TALocationStatusChanged";


//------------------------------------------
// Implementation
//------------------------------------------
#pragma mark - Implementation

@implementation TAAppDelegate

@synthesize lastAccurateLocation;

//------------------------------------------
// UIApplicationDelegate methods
//------------------------------------------
#pragma mark - UIApplicationDelegate methods

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [NewRelicAgent startWithApplicationToken:@"AAbb9d6fe6db60621ebd050cd25c0a75d869568824"];
    
    [Appirater setAppId:APP_STORE_ID];
    [Appirater setUsesUntilPrompt:10];
    [Appirater setDaysUntilPrompt:0.0001];
    [Appirater setSignificantEventsUntilPrompt:-1];
    [Appirater setDelegate:self];
    
    backgroundQueue = dispatch_queue_create("com.ramikay.tari2ak.movementclassifier.background.queue", NULL);
    
    [self updateLocationServicesAndAuthorization];
    
    BOOL _backgroundLocationLaunchValue = [(NSNumber *)[launchOptions objectForKey:UIApplicationLaunchOptionsLocationKey] boolValue];
    
    if (_backgroundLocationLaunchValue == NO)
    {
        [self initializeAnalyticsTracking];
        
        tracker = [[GAI sharedInstance] defaultTracker];
    }
    
    dataRetriever = [TADataRetriever sharedInstance];
    
    standardLocationManager = [[CLLocationManager alloc] init];
    standardLocationManager.delegate = self;
    standardLocationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    
    if ([standardLocationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
    {
        [standardLocationManager requestAlwaysAuthorization];
    }
    
    standardLocationManager.activityType = CLActivityTypeAutomotiveNavigation;
    standardLocationManager.pausesLocationUpdatesAutomatically = YES;
    
    if (locationEnabled && [CLLocationManager significantLocationChangeMonitoringAvailable])
    {
        significantLocationManager = [[CLLocationManager alloc] init];
        significantLocationManager.delegate = self;
        
        [significantLocationManager startMonitoringSignificantLocationChanges];
    }
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    
    if (!_backgroundLocationLaunchValue && self.locationEnabled)
    {
        [Appirater appLaunched:YES];
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    [dataRetriever stopAutoRefresh];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self stopAccurateLocationUpdates];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [self updateLocationServicesAndAuthorization];
    
#ifdef DEBUG
    [dataRetriever startAutoRefresh];
#endif
    
    if (self.locationEnabled || self.authorizationStatus == kCLAuthorizationStatusNotDetermined)
    {
        [dataRetriever startAutoRefresh];
        
        [self startAccurateLocationUpdates];
    }
    else if (self.deniedFullLocationAccess)
    {
        NSUserDefaults *_userDefaults = [NSUserDefaults standardUserDefaults];
        
        // Check how many times the app has run without location access
        NSNumber *_disabledCount = [_userDefaults objectForKey:DISABLED_LOCATION_KEY];
        
        if (_disabledCount)
        {
            NSInteger _disabled = [_disabledCount integerValue];
            _disabledCount = [NSNumber numberWithInteger:++_disabled];
            
            if (_disabled <= ALLOWED_USES_WITHOUT_LOCATION)
            {
                [dataRetriever startAutoRefresh];
            }
        }
        else
        {
            _disabledCount = [NSNumber numberWithInt:1];
        }
        
        [_userDefaults setObject:_disabledCount forKey:DISABLED_LOCATION_KEY];
        [_userDefaults synchronize];
    }
    else
    {
        [dataRetriever startAutoRefresh];
    }
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
//    [self scheduleReminderNotifications];
}

//------------------------------------------
// Public methods
//------------------------------------------
#pragma mark - Public methods

- (CLAuthorizationStatus) authorizationStatus
{
    return authorizationStatus;
}

- (BOOL) deniedFullLocationAccess
{
    return (authorizationStatus == kCLAuthorizationStatusDenied || authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse);
}

- (BOOL) backgroundRefreshRequired
{
    return backgroundRefreshRequired;
}

- (UIBackgroundRefreshStatus) backgroundRefreshStatus
{
    return backgroundRefreshStatus;
}

- (BOOL) locationServicesEnabled
{
    return locationServicesEnabled;
}

- (BOOL) locationEnabled
{
    return locationEnabled;
}

- (CLLocation *) lastAccurationLocation
{
    return lastAccurateLocation;
}

//------------------------------------------
// Private methods
//------------------------------------------
#pragma mark - Private methods

- (void) initializeAnalyticsTracking
{
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    Reachability *_reach = [Reachability reachabilityForInternetConnection];
    
    if (_reach.currentReachabilityStatus == ReachableViaWiFi)
    {
        [GAI sharedInstance].dispatchInterval = DISPATCH_INTERVAL_WIFI;
    }
    else if (_reach.currentReachabilityStatus == ReachableViaWWAN)
    {
        [GAI sharedInstance].dispatchInterval = DISPATCH_INTERVAL_WWAN;
    }
    
    GAILogLevel _logLevel = kGAILogLevelNone;
    
#ifdef DEBUG
//    _logLevel = kGAILogLevelVerbose;
#endif
    
    [[[GAI sharedInstance] logger] setLogLevel:_logLevel];
    
    [[GAI sharedInstance] trackerWithTrackingId:GOOGLE_ANALYTICS_TRACKING_ID];
}

- (void) trackWithCategory:(NSString *)theCategory action:(NSString *)theAction label:(NSString *)theLabel value:(NSNumber *)theValue
{
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:theCategory
                                                          action:theAction
                                                           label:theLabel
                                                           value:theValue] build]];
}

- (void) scheduleReminderNotifications
{
    static NSInteger SECS_PER_DAY = 24*3600;
    
    // One hour after termination time
    NSDate *_1day = [NSDate saneNotificationTimeForDate:[NSDate dateWithTimeIntervalSinceNow:3600] showOnlyInMorning:YES];
    
    [UILocalNotification scheduleNotificationWithMessage:TALocalizedString(@"Remember to check the traffic before you hit the road.")
                                                   sound:YES
                                                  onDate:_1day];
    // 6 days after termination time
    NSDate *_6days = [NSDate saneNotificationTimeForDate:[NSDate dateWithTimeIntervalSinceNow:SECS_PER_DAY*6] showOnlyInMorning:YES];
    
    [UILocalNotification scheduleNotificationWithMessage:TALocalizedString(@"Make sure the roads are not jammed before you head out.")
                                                   sound:YES
                                                  onDate:_6days];
    
    // 15 days after termination time
    NSDate *_15days = [NSDate saneNotificationTimeForDate:[NSDate dateWithTimeIntervalSinceNow:SECS_PER_DAY*15] showOnlyInMorning:YES];

    [UILocalNotification scheduleNotificationWithMessage:TALocalizedString(@"Stop wasting time on the road. Check the traffic before you leave.")
                                                   sound:YES
                                                  onDate:_15days];
    
    // One month after termination time
    NSDate *_30days = [NSDate saneNotificationTimeForDate:[NSDate dateWithTimeIntervalSinceNow:SECS_PER_DAY*30] showOnlyInMorning:YES];
    
    [UILocalNotification scheduleNotificationWithMessage:TALocalizedString(@"There's no reason to be late if you check the traffic.")
                                                   sound:YES
                                                  onDate:_30days];
    
    // 45 days after termination time
    NSDate *_45days = [NSDate saneNotificationTimeForDate:[NSDate dateWithTimeIntervalSinceNow:SECS_PER_DAY*45] showOnlyInMorning:YES];
    
    [UILocalNotification scheduleNotificationWithMessage:TALocalizedString(@"Still getting caught in traffic? Hmmmm...")
                                                   sound:YES
                                                  onDate:_45days];

}

- (BOOL) isInBackground
{
    UIApplication *_app = [UIApplication sharedApplication];
    return (_app.applicationState == UIApplicationStateBackground);
}

- (void) updateLocationServicesAndAuthorization
{
    BOOL _oldServicesEnabled = locationServicesEnabled;
    locationServicesEnabled = [CLLocationManager locationServicesEnabled];
    
    CLAuthorizationStatus _oldAuthorization = authorizationStatus;
    authorizationStatus = [CLLocationManager authorizationStatus];
    
    BOOL _oldLocationEnabled = locationEnabled;
    
    UIApplication *_app = [UIApplication sharedApplication];
    
    backgroundRefreshStatus = [_app backgroundRefreshStatus];
    backgroundRefreshRequired = (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(_iOS_7_0) && SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(_iOS_7_1));
    
    locationEnabled = (locationServicesEnabled == YES && (authorizationStatus == kCLAuthorizationStatusAuthorizedAlways || authorizationStatus == kCLAuthorizationStatusAuthorized) && (!backgroundRefreshRequired || backgroundRefreshStatus == UIBackgroundRefreshStatusAvailable));
    
    if (locationServicesEnabled != _oldServicesEnabled)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:TALocationServicesStatusChanged object:self];
    }
    
    if (authorizationStatus != _oldAuthorization)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:TALocationAuthorizationStatusChanged object:self];
    }
    
    if (_oldLocationEnabled != locationEnabled)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:TALocationStatusChanged object:self];
    }
}

- (void) startAccurateLocationUpdates
{
    lastAccurateLocation = nil;
    beforeLastAccurateLocation = nil;
    
    [standardLocationManager startUpdatingLocation];
}

- (void) stopAccurateLocationUpdates
{
    [standardLocationManager stopUpdatingLocation];
}

- (void) pauseAccurateLocationUpdatesForDuration:(NSTimeInterval)theDuration
{
    [standardLocationManager stopUpdatingLocation];
    [self performSelector:@selector(startAccurateLocationUpdates) withObject:nil afterDelay:theDuration];
}

- (void) beginBackgroundTask
{
    bgTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:
              ^{
                  [[UIApplication sharedApplication] endBackgroundTask:bgTask];
              }];
}

- (void) endBackgroundTask
{
    if (bgTask != UIBackgroundTaskInvalid)
    {
        [[UIApplication sharedApplication] endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }
}

- (void) sendTestReport
{
    CLLocation *_mirnaChalouhi = [[CLLocation alloc] initWithLatitude:33.887945 longitude:35.5509];
    
    TAReport *_report = [[TAReport alloc] initWithCoordinate:_mirnaChalouhi.coordinate
                                                   timestamp:NSDate.date
                                                       speed:6.5
                                                  background:0
                                                  northAngle:35];
    [TADataReporter sendReport:_report success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Sent: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failed: %@", error);
    }];
}

- (void) maybeReportToServer
{
    Reachability *_reachability = [Reachability reachabilityForInternetConnection];
    
    if (_reachability.isReachable && !_reachability.isReachableViaWiFi)
    {
        if ([CMMotionActivityManager isActivityAvailable])
        {
            NSOperationQueue *_activityQueue = [NSOperationQueue new];
            CMMotionActivityManager *_activityManager = [[CMMotionActivityManager alloc] init];
            
            [_activityManager startActivityUpdatesToQueue:_activityQueue withHandler:^(CMMotionActivity *activity) {
                if (activity.automotive == YES)
                {
#ifdef DEBUG
                    [UILocalNotification showNotificationWithMessage:@"Automotive using M7" sound:YES];
#endif
                    [self dispatch];
                }
                else
                {
                    [self performActivityCheck];
                }
                
                [_activityManager stopActivityUpdates];
            }];
        }
        else
        {
            [self performActivityCheck];
        }
    }
}

- (void) performActivityCheck
{
    if (![RKMovementClassifier sharedInstance].detectionInProgress)
    {
        dispatch_async(backgroundQueue, ^(void)
        {
           [[RKMovementClassifier sharedInstance] detectMovementWithCompletionBlock:^(NSArray *probabilities, RKMovementType movement)
            {
                double _vehicleProbability = [[probabilities objectAtIndex:RKMovementTypeVehicle] doubleValue];
                double _motorbikeProbability = [[probabilities objectAtIndex:RKMovementTypeMotorbike] doubleValue];
                double _automotiveProbability = _vehicleProbability + _motorbikeProbability;
                
#ifdef DEBUG
                NSString *_probas = [NSString stringWithFormat:@"Type:%d, Automotive:%.2f", movement, _automotiveProbability];
                NSLog(@"%@", _probas);
                [UILocalNotification showNotificationWithMessage:_probas sound:YES];
#endif
                
                if (_automotiveProbability >= 0.5)
                {
                    // Report
                    [self dispatch];
                }
                else
                {
                    [self endBackgroundTask];
                }
            }];
        });
    }
}

- (void) dispatch
{
    float _calculatedSpeed = [lastAccurateLocation speedFromLocation:beforeLastAccurateLocation];
    float _managerSpeed = lastAccurateLocation.speed;
    float _speed = (_managerSpeed * LOW_PASS_FILTER) + (1-LOW_PASS_FILTER) * _calculatedSpeed;
    
#ifdef DEBUG
    NSString *_speeds = [NSString stringWithFormat:@"Speed: %.2f", _speed*3.6];
    NSLog(@"%@", _speeds);
    [UILocalNotification showNotificationWithMessage:_speeds sound:NO];
#endif
    
    __block TAAppDelegate *_safeSelf = self;
    
    double _northAngle = [beforeLastAccurateLocation northAngleWithLocation:lastAccurateLocation];
    
    TAReport *_report = [[TAReport alloc] initWithCoordinate:lastAccurateLocation.coordinate
                                                   timestamp:lastAccurateLocation.timestamp
                                                       speed:_speed
                                                  background:self.isInBackground
                                                  northAngle:_northAngle];
    
    [TADataReporter sendReport:_report
                       success:^(AFHTTPRequestOperation *theOperation, id theResponseObject)
     {
#ifdef DEBUG
         [UILocalNotification showNotificationWithMessage:@"Successfully reported to server" sound:NO];
#endif
         // Analytics Event
         [self trackWithCategory:@"Reporting" action:@"Successful Report" label:nil value:nil];
         
         if ([_safeSelf isInBackground])
         {
             [self endBackgroundTask];
         }
     }
                       failure:^(AFHTTPRequestOperation *theOperation, NSError *theError)
     {
#ifdef DEBUG
         NSString *_errorMessage = [NSString stringWithFormat:@"Error reporting to server: %d", theOperation.response.statusCode];
         [UILocalNotification showNotificationWithMessage:_errorMessage sound:YES];
#endif
         
         // Analytics Event
         [self trackWithCategory:@"Reporting" action:@"Failed Report" label:theError.description value:[NSNumber numberWithInteger:theOperation.response.statusCode]];
         
         if ([_safeSelf isInBackground])
         {
             [self endBackgroundTask];
         }
     }];
}

//------------------------------------------
// CLLocationManagerDelegate methods
//------------------------------------------
#pragma mark - CLLocationManagerDelegate methods

- (void)locationManager:(CLLocationManager *)theManager didUpdateLocations:(NSArray *)theLocations
{
    if (theManager == standardLocationManager)
    {
        CLLocation *_location = [theLocations lastObject];
        
        NSTimeInterval _secondsAgo = [_location.timestamp timeIntervalSinceNow];
        
        // Filter out "inital fix" locations and cached locations
        if (_location.speed >= 0 && _location.horizontalAccuracy < 10 && abs(_secondsAgo) < 5)
        {
            if (lastAccurateLocation == nil)
            {
                lastAccurateLocation = _location;
            }
            else if (beforeLastAccurateLocation == nil && [_location distanceFromLocation:lastAccurateLocation] >= 5)
            {
                beforeLastAccurateLocation = lastAccurateLocation;
                lastAccurateLocation = _location;
                
                [self maybeReportToServer];
                
                if (self.isInBackground)
                {
                    [self stopAccurateLocationUpdates];
                }
                else
                {
                    [self pauseAccurateLocationUpdatesForDuration:120];
                }
            }
            else
            {
                [self stopAccurateLocationUpdates];
            }
        }
    }
    else
    {
        if (self.isInBackground)
        {
            [self beginBackgroundTask];
            
            [self startAccurateLocationUpdates];
            
            // Analytics Event
            [self trackWithCategory:@"Reporting" action:@"Woke Up" label:nil value:nil];
        }
    }
}

- (void) locationManager:(CLLocationManager *)theManager didChangeAuthorizationStatus:(CLAuthorizationStatus)aStatus
{
    [self updateLocationServicesAndAuthorization];
}

@end
