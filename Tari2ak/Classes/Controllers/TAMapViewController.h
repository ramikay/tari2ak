//
//  TAMapViewController.h
//  Tari2ak
//
//  Created by Rami on 27/3/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "GAITrackedViewController.h"

@class TAStreet, TADataRetriever;

@interface TAMapViewController : GAITrackedViewController <MKMapViewDelegate, UIAlertViewDelegate, UISearchBarDelegate, UIActionSheetDelegate>
{
    TADataRetriever *dataRetriever;
}

@property (strong, nonatomic) IBOutlet MKMapView *mapView;

@property (strong, nonatomic) IBOutlet UILabel *watermark;
@property (strong, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) IBOutlet UIView *loadingContainer;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UILabel *loadingLabel;

@property (assign, nonatomic) BOOL didLoadMap;
@property (strong, nonatomic) IBOutlet UIButton *refreshButton;
@property (strong, nonatomic) IBOutlet UIButton *locationButton;
@property (strong, nonatomic) IBOutlet UIButton *typeButton;

@property (strong, nonatomic) IBOutlet UIView *searchContainer;
@property (strong, nonatomic) IBOutlet UITableView *searchResultsTable;

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

- (void) beginPolylineAnimationAfter:(NSTimeInterval)delay;
- (void) stopPolylineAnimation;

- (IBAction) refreshAction:(id)theSender;
- (IBAction) locationAction:(id)theSender;
- (IBAction) typeAction:(id)theSender;

- (void) showAnnotationForStreet:(TAStreet *)aStreet title:(NSString *)theTitle subtitle:(NSString *)theSubtitle;

@end
