//
//  TATableViewController.m
//  Tari2ak
//
//  Created by Rami Khawandi on 181/6/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import "TATableViewController.h"
#import "TAStreet.h"
#import "TAStreetCell.h"
#import "TAConstants.h"
#import "ODRefreshControl.h"
#import "NSBundle+Helpers.h"
#import "UILabel+Arabic.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"
#import "NSDate+RelativeTime.h"
#import "TALocationDataSource.h"
#import "TAMapViewController.h"
#import "ArabicConverter.h"

@interface TATableViewController ()
{
    NSArray *searchResults;
    NSMutableArray *streets;
    UIButton *cancelButton;
    
    TALocaleManager *localeManager;
    id tracker;
}

- (void) reloadLocale;
- (void) updateAllStreets;
- (void) sortStreets;
- (void) filterStreetsByName:(NSString *)theString;
- (UIColor *) colorForStreet:(TAStreet *)theStreet;
- (UIColor *) colorForCellAtIndexPath:(NSIndexPath *)theIndexPath;
- (void) popToMapAndHighlightStreet:(TAStreet *)theStreet;
- (NSString *) localizedNameForStreet:(TAStreet *)theStreet;

@end

@implementation TATableViewController

@synthesize tableView;

//------------------------------------------
// View lifecycle
//------------------------------------------
#pragma mark - View lifecycle

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    dataRetriever = [TADataRetriever sharedInstance];
    streets = [[NSMutableArray alloc] init];
    
    localeManager = [TALocaleManager sharedInstance];
    
    tracker = [[GAI sharedInstance] defaultTracker];
    
    [TALocationDataSource sharedInstance];
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor whiteColor]];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    refreshControl = [[ODRefreshControl alloc] initInScrollView:self.tableView];
    refreshControl.tintColor = RGB(255, 197, 23);
    refreshControl.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    
    [refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
    
    self.searchDisplayController.delegate = self;
    self.searchDisplayController.searchResultsDataSource = self;
    self.searchDisplayController.searchResultsDelegate = self;
    
    self.searchDisplayController.searchResultsTableView.backgroundColor = RGB(82,82,82);
    self.searchDisplayController.searchResultsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    NSNotificationCenter *_nc = [NSNotificationCenter defaultCenter];
    [_nc addObserver:self selector:@selector(didUpdateUserLocation:) name:TAUserLocationChanged object:nil];
    
    [_nc addObserver:self selector:@selector(didStartUpdate:) name:TADataRetrieverUpdateBegan object:nil];
    [_nc addObserver:self selector:@selector(didUpdateStreets:) name:TADataRetrieverStreetsUpdateComplete object:nil];
    [_nc addObserver:self selector:@selector(didReceiveError:) name:TADataRetrieverUpdateFailed object:nil];
    [_nc addObserver:self selector:@selector(reloadLocale) name:TALocaleChanged object:nil];
}

- (void) viewWillAppear:(BOOL)isAnimated
{
    [super viewWillAppear:isAnimated];
    
    [self reloadLocale];
}

- (void) viewDidAppear:(BOOL)isAnimated
{
    [tracker set:kGAIScreenName value:@"Roads"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    if (dataRetriever.updateInProgress)
    {
        [refreshControl beginRefreshing];
    }
    else
    {
        [refreshControl endRefreshing];
        [self updateAllStreets];
        [self.tableView reloadData];
    }
}

- (void) viewDidDisappear:(BOOL)isAimated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewDidDisappear:isAimated];
}

//------------------------------------------
// UIViewController methods
//------------------------------------------
#pragma mark - UIViewController methods

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL) shouldAutorotate
{
    return NO;
}

- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

//------------------------------------------
// Private methods
//------------------------------------------
#pragma mark - Private methods

- (void) popToMapAndHighlightStreet:(TAStreet *)theStreet
{
    TAMapViewController *_mapViewController = (TAMapViewController *) [self.navigationController.viewControllers objectAtIndex:0];
    [self.navigationController popToViewController:_mapViewController animated:YES];
    
    NSString *_timeAgo = TALocalizedString(theStreet.lastModificationDate.timeAgoSinceNow);
    NSInteger _speed  = round(theStreet.speed);
    
    NSString *_subtitle = [NSString stringWithFormat:@"%d km/h  %@", _speed, _timeAgo];
    
    [_mapViewController showAnnotationForStreet:theStreet title:[self localizedNameForStreet:theStreet] subtitle:_subtitle];
}


- (void) reloadLocale
{
    BOOL _isArabic = [localeManager.locale isEqualToString:LOCALE_ARABIC];
    
    CGFloat _titleFontSize = (_isArabic) ? 36 : 20;
    NSString *_fontName = (_isArabic) ? @"BArabicStyle" : @"HelveticaNeue-Bold";
    NSValue *_shadowOffset = (_isArabic) ? [NSValue valueWithUIOffset:UIOffsetMake(1, 1)] : [NSValue valueWithUIOffset:UIOffsetZero];
    
    self.navigationItem.title = [ArabicConverter convertArabic:TALocalizedString(@"Roads")];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor whiteColor],UITextAttributeTextColor,
                                                                     [UIColor blackColor], UITextAttributeTextShadowColor,
                                                                     _shadowOffset,UITextAttributeTextShadowOffset,
                                                                     [UIFont fontWithName:_fontName size:_titleFontSize], UITextAttributeFont, nil]];
    
    self.searchDisplayController.searchBar.placeholder = TALocalizedString(@"Search streets using any language");
    
    [self.tableView reloadData];
    
}

- (void) updateAllStreets
{
    [streets removeAllObjects];
    
    NSArray *_streets = [dataRetriever.streets allValues];
    
    //  Not using predicates since manual filtering seems to be cheaper on CPU
//    NSPredicate *_predicate = [NSPredicate predicateWithFormat:@"arabicName.length > 0 OR englishName.length > 0 OR frenchName.length > 0"];
//    streets = [[_streets filteredArrayUsingPredicate:_predicate] mutableCopy];
    
    for (TAStreet *street in _streets)
    {
        NSArray *_streetNames = [street.names allValues];
        
        for (NSString *name in _streetNames)
        {
            if (name.length > 0)
            {
                [streets addObject:street];
                break;
            }
        }
    }
    
    [self sortStreets];
}

- (void) filterStreetsByName:(NSString *)theString
{
    // Remove digits from search string
    NSCharacterSet *_noDigits = [NSCharacterSet decimalDigitCharacterSet];
    NSString *_searchQuery = [[theString componentsSeparatedByCharactersInSet:_noDigits] componentsJoinedByString:@""];
    
    NSPredicate *_predicate = [NSPredicate
                                    predicateWithFormat:@"arabicName contains[cd] %@ OR englishName contains[cd] %@ OR frenchName contains[cd] %@",
                                    _searchQuery, _searchQuery, _searchQuery];

    searchResults = [streets filteredArrayUsingPredicate:_predicate];
    
    [self sortStreets];
}

- (void) sortStreets
{
    NSSortDescriptor *_proximityDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distanceFromUser" ascending:YES];
    NSSortDescriptor *_timeDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastModificationDate" ascending:NO];
    NSArray *_sortDescriptors = @[_proximityDescriptor, _timeDescriptor];
    
    [streets sortUsingDescriptors:_sortDescriptors];
}

- (UIColor *) colorForCellAtIndexPath:(NSIndexPath *)theIndexPath
{
    NSInteger _row = theIndexPath.row;
    
    if (_row%2 == 0)
    {
        return RGB(113,113,113);
    }
    else
    {
        return RGB(82,82,82);
    }
}

- (UIColor *) colorForStreet:(TAStreet *)theStreet
{
    if (theStreet.condition == TATrafficConditionHeavy || theStreet.condition == TATrafficConditionVeryHeavy)
    {
        return RGB(255,22,28);
    }
    else if (theStreet.condition == TATrafficConditionMedium)
    {
        return RGB(254,196,52);
    }
    else
    {
        return RGB(78,226,96);
    }
}

- (NSString *) localizedNameForStreet:(TAStreet *)theStreet
{
    NSString *_lang = localeManager.languageID;
    NSString *_name = [theStreet.names objectForKey:localeManager.languageID];
    
    NSArray *_values    = [theStreet.names allValues];
    NSArray *_keys      = [theStreet.names allKeys];
    
    // Try to find an English name first
    if (_name.length == 0)
    {
        _name = [theStreet.names objectForKey:LOCALE_ENGLISH];
        _lang = LOCALE_ENGLISH;
    }
    
    // Search for whatever name is available
    int _i = 0;
    while (_name.length == 0)
    {
        if (_i > ([theStreet.names count] -1))
        {
            break;
        }
        else
        {
            _name = [_values objectAtIndex:_i];
            _lang = [_keys objectAtIndex:_i];
            _i++;
        }
    }
    
    return _name;
}

//------------------------------------------
// Actions
//------------------------------------------
#pragma mark - Actions

- (IBAction) popToMapViewController:(id)theSender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction) didTapInfoButton:(id)theSender
{
    [self.navigationController performSegueWithIdentifier:@"GoToInfo" sender:self];
}

- (void) didUpdateUserLocation:(NSNotification *)theNotification
{
    if (streets.count > 1)
    {
        [self sortStreets];
        [self.tableView reloadData];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TAUserLocationChanged object:nil];
}

- (void)dropViewDidBeginRefreshing:(ODRefreshControl *)refreshControl
{
    [dataRetriever update];
}

- (void) didStartUpdate:(NSNotification *)theNotification
{
    [refreshControl beginRefreshing];
}

- (void) didUpdateStreets:(NSNotification *)theNotification
{
    [self updateAllStreets];
    [self.tableView reloadData];
    
    [refreshControl endRefreshing];
}

- (void) didReceiveError:(NSNotification *)theNotification
{
    [refreshControl endRefreshing];
}

//------------------------------------------
// UITableViewDataSource methods
//------------------------------------------
#pragma mark - UITableViewDataSource methods

- (NSInteger) tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)theSection
{
    NSUInteger _rows = (theTableView == self.searchDisplayController.searchResultsTableView) ? [searchResults count] : [streets count];
    
    if (_rows < 8) { _rows = 7; }
    
    return _rows;
}

- (UITableViewCell *) tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)theIndexPath
{
    NSInteger _row = theIndexPath.row;
    NSArray *_source = (theTableView == self.searchDisplayController.searchResultsTableView) ? searchResults : streets;
    
    TAStreetCell *_cell;
    NSInteger _lastCellIndex = [self tableView:theTableView numberOfRowsInSection:theIndexPath.section] - 1;
    BOOL _isBottomCell = ([_source count] > 0 && _row == _lastCellIndex);
    
    // Last cell
    if (_isBottomCell)
    {
        _cell = [theTableView dequeueReusableCellWithIdentifier:@"BottomStreetCell"];
        
        if (_cell == nil)
        {
            _cell = [[NSBundle mainBundle] loadObjectFromNibNamed:@"TABottomStreetCell" class:[TAStreetCell class] owner:self options:nil];
        }
        
        _cell.contentView.backgroundColor = [self colorForCellAtIndexPath:theIndexPath];
        _cell.streetName.text = TALocalizedString(@"Roads are sorted by proximity to you");
    }
    else
    {
        if (_row < [_source count])
        {
            TAStreet *_street = [_source objectAtIndex:_row];
            
            NSString *_lang = localeManager.languageID;
            NSString *_name = [_street.names objectForKey:localeManager.languageID];
            
            NSArray *_values    = [_street.names allValues];
            NSArray *_keys      = [_street.names allKeys];
            
            // Try to find an English name first
            if (_name.length == 0)
            {
                _name = [_street.names objectForKey:LOCALE_ENGLISH];
                _lang = LOCALE_ENGLISH;
            }
            
            // Search for whatever name is available
            int _i = 0;
            while (_name.length == 0)
            {
                if (_i > ([_street.names count] -1))
                {
                    break;
                }
                else
                {
                    _name = [_values objectAtIndex:_i];
                    _lang = [_keys objectAtIndex:_i];
                    _i++;
                }
            }
            
            if ([localeManager.languageID isEqualToString:LOCALE_ARABIC])
            {
                _cell = [theTableView dequeueReusableCellWithIdentifier:@"TAStreetCell-RTL"];
                
                if (_cell == nil)
                {
                    _cell = [[NSBundle mainBundle] loadObjectFromNibNamed:@"TAStreetCell-RTL" class:[TAStreetCell class] owner:self options:nil];
                }
            }
            else
            {
                _cell = [theTableView dequeueReusableCellWithIdentifier:@"TAStreetCell-LTR"];
                
                if (_cell == nil)
                {
                    _cell = [[NSBundle mainBundle] loadObjectFromNibNamed:@"TAStreetCell-LTR" class:[TAStreetCell class] owner:self options:nil];
                }
            }
            
            if ([_lang isEqualToString:LOCALE_ARABIC])
            {
                _cell.streetName.font = [UIFont fontWithName:@"BArabicStyle" size:32];
            }
            else
            {
                _cell.streetName.font = [UIFont boldSystemFontOfSize:20];
            }
            
            _cell.contentView.backgroundColor = [self colorForCellAtIndexPath:theIndexPath];
            
            NSInteger _speed  = round(_street.speed);
            double _distance = round(_street.distanceFromUser) / 1024;
            
//            [_cell.streetName setArabicText:@"حج أكفي اللّكح هوي"];   // Used for debugging Arabic font
            [_cell.streetName setArabicText:_name];
            _cell.speedIndicator.hidden = NO;
            _cell.timeIndicator.hidden = NO;
            _cell.proximityIndicator.hidden = (_distance < 0);
            
            _cell.trafficIndicator.backgroundColor = [self colorForStreet:_street];
            
            _cell.averageSpeed.text = [NSString stringWithFormat:@"%d km/h", _speed];
            _cell.lastUpdated.text = TALocalizedString(_street.lastModificationDate.timeAgoSinceNow);
            _cell.proximity.text = (_distance < 0) ? @"" : [NSString stringWithFormat:@"%.2f km", _distance];
        }
        else
        {
            _cell = [theTableView dequeueReusableCellWithIdentifier:@"TAStreetCell-LTR"];
            
            if (_cell == nil)
            {
                _cell = [[NSBundle mainBundle] loadObjectFromNibNamed:@"TAStreetCell-LTR" class:[TAStreetCell class] owner:self options:nil];
            }
            
            _cell.contentView.backgroundColor = [self colorForCellAtIndexPath:theIndexPath];
            
            _cell.speedIndicator.hidden = YES;
            _cell.timeIndicator.hidden = YES;
            _cell.proximityIndicator.hidden = YES;
            
            _cell.trafficIndicator.backgroundColor = UIColor.clearColor;
            
            [_cell.streetName setText:@""];
            [_cell.averageSpeed setText:@""];
            [_cell.lastUpdated setText:@""];
            [_cell.proximity setText:@""];
        }
    }

    return _cell;
}

//------------------------------------------
// UITableViewDelegate methods
//------------------------------------------
#pragma mark - UITableViewDelegate methods

- (CGFloat) tableView:(UITableView *)theTableView heightForRowAtIndexPath:(NSIndexPath *)theIndexPath
{
    NSInteger _row = theIndexPath.row;
    NSArray *_source = (theTableView == self.searchDisplayController.searchResultsTableView) ? searchResults : streets;
    NSInteger _lastCellIndex = [self tableView:theTableView numberOfRowsInSection:theIndexPath.section] - 1;
    BOOL _isBottomCell = ([_source count] > 0 && _row == _lastCellIndex);
    return (_isBottomCell) ? 40 : 63;
}

- (void) tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)anIndexPath
{
    NSInteger _row = anIndexPath.row;
    NSArray *_source = (theTableView == self.searchDisplayController.searchResultsTableView) ? searchResults : streets;
    
    NSInteger _lastCellIndex = [self tableView:theTableView numberOfRowsInSection:anIndexPath.section] - 1;
    BOOL _isBottomCell = ([_source count] > 0 && _row == _lastCellIndex);
    
    // Last cell
    if (!_isBottomCell)
    {
        if (_row < [_source count])
        {
            TAStreet *_street = [_source objectAtIndex:_row];
            
            [self popToMapAndHighlightStreet:_street];
        }
    }
}

//------------------------------------------
// UISearchDisplayDelegate methods
//------------------------------------------
#pragma mark - UISearchDisplayDelegate methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)theController shouldReloadTableForSearchString:(NSString *)theSearchString
{
    [self filterStreetsByName:theSearchString];
    
    return YES;
}

-(void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
    self.searchDisplayController.searchBar.placeholder = @"";
    
    if ([localeManager.locale isEqualToString:LOCALE_ARABIC] && IS_IOS_7_OR_NEWER)
    {
        self.searchDisplayController.searchBar.showsCancelButton = YES;

        UIView *_topView = self.searchDisplayController.searchBar.subviews[0];

        for (UIView *subView in _topView.subviews)
        {
            if ([subView isKindOfClass:NSClassFromString(@"UINavigationButton")])
            {
                cancelButton = (UIButton*)subView;
            }
        }

        if (cancelButton)
        {
            [cancelButton setTitle:TALocalizedString(@"Cancel") forState:UIControlStateNormal];
        }
    }
}

- (void) searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller
{
    // Analytics Event
    NSString *_searchQuery = controller.searchBar.text;
    NSNumber *_resultCount = [NSNumber numberWithInteger:searchResults.count];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Data"
                                                          action:@"Search"
                                                           label:_searchQuery
                                                           value:_resultCount] build]];
}

- (void) searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller
{
    self.searchDisplayController.searchBar.placeholder = TALocalizedString(@"Search streets using any language");
}

@end
