//
//  TAInfoViewConroller.m
//  Tari2ak
//
//  Created by Rami Khawandi on 1/12/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import "TAInfoViewConroller.h"
#import "UINavigationController+CustomTransitions.h"
#import "TAConstants.h"
#import "TATextViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "TACopyLinkActivity.h"
#import "JBWhatsAppActivity.h"
#import "ArabicConverter.h"

@interface TAInfoViewConroller ()
{
    TALocaleManager *localeManager;
    id tracker;
}

- (CGFloat) heightForHeaders;

- (void) trackEventWithAction:(NSString *)theAction label:(NSString *)theLabel;

- (void) showPicker;
- (void) hidePicker;
- (void) togglePicker;

- (void) shareApp;
- (void) showAppOnStore;
- (void) showFacebookPage;
- (void) showTwitterPage;

@end

@implementation TAInfoViewConroller

@synthesize pickerBlocker, pickerView, tableView, tap;

//------------------------------------------
// View lifecycle
//------------------------------------------
#pragma mark - View lifecycle

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    localeManager = [TALocaleManager sharedInstance];
    
    tracker = [[GAI sharedInstance] defaultTracker];
    self.screenName = @"Info";
    
    self.navigationItem.hidesBackButton = YES;
    
    UIButton *_closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    _closeButton.showsTouchWhenHighlighted = YES;
    [_closeButton setImage:[UIImage imageNamed:@"close-button.png"] forState:UIControlStateNormal];
    [_closeButton addTarget:self action:@selector(didTapCloseButton:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_closeButton];
    
    if ([localeManager.locale isEqualToString:LOCALE_ENGLISH])
    {
        [pickerView selectRow:1 inComponent:0 animated:NO];
    }
    else if ([localeManager.locale isEqualToString:LOCALE_FRENCH])
    {
        [pickerView selectRow:2 inComponent:0 animated:NO];
    }
    else if ([localeManager.locale isEqualToString:LOCALE_ARMENIAN])
    {
        [pickerView selectRow:3 inComponent:0 animated:NO];
    }
    
    // Title Label
    UILabel *_titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    _titleLabel.font = [UIFont fontWithName:@"BArabicStyle" size:38];
    _titleLabel.textColor = UIColor.whiteColor;
    _titleLabel.shadowColor = UIColor.blackColor;
    _titleLabel.text = [ArabicConverter convertArabic:@"طريقك"];
    _titleLabel.shadowOffset = CGSizeMake(1, 1);
    [_titleLabel sizeToFit];
    self.navigationItem.titleView = _titleLabel;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void) viewWillAppear:(BOOL)isAnimated
{
    [super viewWillAppear:isAnimated];
    
    [tableView deselectRowAtIndexPath:tableView.indexPathForSelectedRow animated:NO];
}

- (void)prepareForSegue:(UIStoryboardSegue *)theSegue sender:(id)theSender
{
    if ([theSegue.identifier isEqualToString:@"GoToTextView"])
    {
        NSInteger _selectedCellRow = [theSender integerValue];
        TATextViewController *_textViewController = (TATextViewController *) theSegue.destinationViewController;
        
        switch (_selectedCellRow)
        {
            // How it works
            case 0:
                _textViewController.text = @"Tari'ak uses smartphones from around you as traffic sensors by monitoring their movement to learn when they're in automotive transport, how fast they’re moving, and which road they are on.\n\nThis information is instantly sent to a server on the cloud which computes the average speed on each road, and makes it available for users like you.\n\nRest assured that while Tari'ak does measure your device’s speed and location when it is in automotive transport (like a moving car or bus,) it does not collect any personal information or data that can tell who you are.\n\nThe app does not ask for any of your name, e-mail, phone number, or social media accounts. No registration or login is required to use the app.\n\nTari-ak takes full care in preserving your battery life by responsibly using the GPS only upon need. The app turns on Location Services for a few seconds only when you move a significant distance, meaning it will not consume any battery power if your phone is sitting on a desk for example. Mobile data use is also very minimal.";
                break;
            
            // Privacy policy
            case 1:
                _textViewController.text = @"Tari'ak does not collect any personal information that can identify who you are. The app does not ask for any of your name, e-mail, phone number, or social media accounts.\n\nNo registration or login is required to use the app. Using Tari'ak is completely anonymous.\n\nThe following is a list of the data collected by the app:\n\n1. Location Data: In order to measure traffic, the app uses location data provided by the device in order to figure out which road a user is on, as well as how fast the user is moving. This information is collected with complete anonymity.\n\nA user’s location is also used in order to enable the tracking mode feature.\n\n2. Usage Analytics: for the sake of improving the user experience, the app anonymously aggregates a number of user behavior events, as well as errors, and reports them to an analytics service\n\nFor more information or enquiries, please contact the team at team@tari2ak.com";
                break;
        }
    }
}

//------------------------------------------
// UIViewController methods
//------------------------------------------
#pragma mark - UIViewController methods

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL) shouldAutorotate
{
    return NO;
}

- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

//------------------------------------------
// UITableViewDataSource methods
//------------------------------------------
#pragma mark - UITableViewDataSource methods

- (NSInteger) numberOfSectionsInTableView:(UITableView *)theTableView
{
    return 3;
}

- (NSInteger) tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)aSection
{
    switch(aSection)
    {
        case 0:
            return 1;
            break;
            
        case 1:
            return 3;
            break;
            
        case 2:
            return 4;
            break;
            
        default:
            return 0;
    }
}

- (UITableViewCell *) tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)anIndexPath
{
    NSInteger _section = anIndexPath.section;
    NSInteger _row = anIndexPath.row;
    NSString *reuseIdentifier = (_row == 0) ? @"LanguageCell" : @"InfoCell";
    UITableViewCell *_cell = [theTableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    if (!_cell)
    {
        if (_section == 0 && _row == 0)
        {
            _cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"LanguageCell"];
            _cell.textLabel.text = @"Interface Language";
            _cell.detailTextLabel.textColor = RGB(0,122,255);
            
            if ([localeManager.locale  isEqualToString:LOCALE_ARABIC])
            {
                _cell.detailTextLabel.text = @"العربية";
            }
            else if ([localeManager.locale  isEqualToString:LOCALE_ENGLISH])
            {
                _cell.detailTextLabel.text = @"English";
            }
            else if ([localeManager.locale  isEqualToString:LOCALE_FRENCH])
            {
                _cell.detailTextLabel.text = @"Français";
            }
            else if ([localeManager.locale  isEqualToString:LOCALE_ARMENIAN])
            {
                _cell.detailTextLabel.text = @"հայերեն";
            }
        }
        else
        {
            _cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InfoCell"];
            _cell.backgroundColor = [UIColor whiteColor];
            _cell.accessoryType = (_section == 1 && _row < 2) ? UITableViewCellAccessoryDisclosureIndicator : UITableViewCellAccessoryNone;
        }
        
        _cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    }
    
    switch (_section) {
        case 1:
            switch (_row)
            {
            case 0:
                _cell.textLabel.text = @"How it Works";
                break;
                
            case 1:
                _cell.textLabel.text = @"Privacy Policy";
                break;
                
            case 2:
                _cell.textLabel.text = @"Contact the Team";
                break;
            }
            
            break;
            
        case 2:
            switch (_row)
            {
                case 0:
                    _cell.textLabel.text = @"Share the App";
                    break;
                    
                case 1:
                    _cell.textLabel.text = @"Rate on App Store";
                    break;
                    
                case 2:
                    _cell.textLabel.text = @"Like on Facebook";
                    break;
                    
                case 3:
                    _cell.textLabel.text = @"Follow on Twitter";
                    break;
            }
            break;
            
        default:
            break;
    }
    
    return _cell;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGFloat _width = [UIScreen mainScreen].bounds.size.width;
    CGFloat _height = [self heightForHeaders];
    UIView *_header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _width, _height)];
    _header.backgroundColor = RGB(223, 223, 223);
    
    return _header;
}

- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    CGFloat _width = [UIScreen mainScreen].bounds.size.width;
    CGFloat _height = [self heightForHeaders];
    UIView *_header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _width, _height)];
    _header.backgroundColor = RGB(223, 223, 223);
    
    return _header;
}

//------------------------------------------
// UITableViewDelegate methods
//------------------------------------------
#pragma mark - UITableViewDelegate methods

- (CGFloat) tableView:(UITableView *)theTableView heightForHeaderInSection:(NSInteger)section
{
    return [self heightForHeaders];
}

- (CGFloat) tableView:(UITableView *)theTableView heightForFooterInSection:(NSInteger)section
{
    if (section == 2)
    {
        return [self heightForHeaders];
    }
    else
    {
        return 0;
    }
}

- (NSIndexPath *) tableView:(UITableView *)theTableView willSelectRowAtIndexPath:(NSIndexPath *)anIndexPath
{
    switch (anIndexPath.section)
    {
        case 0:
            [self togglePicker];
            break;
            
        case 1:
            switch (anIndexPath.row)
        {
            case 0:
                [self performSegueWithIdentifier:@"GoToTextView" sender:[NSNumber numberWithInteger:anIndexPath.row]];
                
                // Analytics Event
                [self trackEventWithAction:@"Button Press" label:@"How it Works"];
                break;
                
            case 1:
                [self performSegueWithIdentifier:@"GoToTextView" sender:[NSNumber numberWithInteger:anIndexPath.row]];
                
                // Analytics Event
                [self trackEventWithAction:@"Button Press" label:@"Privacy Policy"];
                break;
                
            case 2:
                if ([MFMailComposeViewController canSendMail])
                {
                    MFMailComposeViewController *_controller = [[MFMailComposeViewController alloc] init];
                    _controller.mailComposeDelegate = self;
                    [_controller setSubject:@"Hello"];
                    [_controller setToRecipients:[NSArray arrayWithObject:@"team@tari2ak.com"]];
                    
                    [self.navigationController presentViewController:_controller animated:YES completion:nil];
                }
                
                // Analytics Event
                [self trackEventWithAction:@"Button Press" label:@"Contact the Team"];
                break;
        }
            
            break;
            
        case 2:
            switch (anIndexPath.row)
        {
            case 0:
                [self shareApp];
                break;
                
            case 1:
                [self showAppOnStore];
                break;
                
            case 2:
                [self showFacebookPage];
                break;
                
            case 3:
                [self showTwitterPage];
                break;
        }
            break;
            
        default:
            break;
    }
    
    return anIndexPath;
}

//------------------------------------------
// UIPickerViewDataSource methods
//------------------------------------------
#pragma mark - UIPickerViewDataSource methods

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)thePickerView
{
    return 1;
}

- (NSInteger) pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)theComponent
{
    return 4;
}

//------------------------------------------
// UIPickerViewDelegate methods
//------------------------------------------
#pragma mark - UIPickerViewDelegate methods

- (CGFloat) pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 44;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)theRow forComponent:(NSInteger)theComponent
{
    switch (theRow)
    {
        case 0:
            return @"العربية";
            break;
            
        case 1:
            return @"English";
            break;
            
        case 2:
            return @"Français";
            break;
            
        case 3:
            return @"հայերեն";
            break;
            
        default:
            return nil;
            break;
    }
}

- (void) pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)aRow inComponent:(NSInteger)aComponent
{
    UITableViewCell *_languageCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    NSString *_language = nil;
    
    switch (aRow)
    {
        case 0:
            localeManager.locale = LOCALE_ARABIC;
            _language = @"العربية";
            _languageCell.detailTextLabel.text = _language;
            break;
            
        case 1:
            localeManager.locale = LOCALE_ENGLISH;
            _language = @"English";
            break;
            
        case 2:
            localeManager.locale = LOCALE_FRENCH;
            _language = @"Français";
            break;
            
        case 3:
            localeManager.locale = LOCALE_ARMENIAN;
            _language = @"հայերեն";
            break;
            
        default:
            break;
    }
    
    _languageCell.detailTextLabel.text = _language;
    
    // Analytics Event
    [self trackEventWithAction:@"Changed Language" label:_language];
}

//------------------------------------------
// MFMailComposeViewControllerDelegate
//------------------------------------------
#pragma mark - MFMailComposeViewControllerDelegate methods

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

//------------------------------------------
// SKStoreProductViewControllerDelegate
//------------------------------------------
#pragma mark - SKStoreProductViewControllerDelegate methods

- (void) productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

//------------------------------------------
// Actions
//------------------------------------------
#pragma mark - Actions

- (void) didEnterForeground:(NSNotification *)theNotification
{
    [self.tableView deselectRowAtIndexPath:tableView.indexPathForSelectedRow animated:NO];
}

- (IBAction) tapOutsidePicker:(id)theSender
{
    UITapGestureRecognizer *_tap = (UITapGestureRecognizer *) theSender;
    CGPoint _point = [_tap locationInView:pickerView];
    BOOL _isTouchInPickerView = (_point.y > 0);
    
    if (!_isTouchInPickerView && !pickerView.hidden)
    {
        [self hidePicker];
    }
}

- (void) didTapCloseButton:(id)theSender
{
    [self.navigationController popViewControllerUsingTransition:kCATransitionReveal
                                              transitionSubType:kCATransitionFromBottom
                                                       duration:0.3];
}

//------------------------------------------
// Private methods
//------------------------------------------
#pragma mark - Private methods

- (void) trackEventWithAction:(NSString *)theAction label:(NSString *)theLabel
{
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Info"
                                                          action:theAction
                                                           label:theLabel
                                                           value:nil] build]];
}

- (CGFloat) heightForHeaders
{
    CGFloat _screenHeight = [UIScreen mainScreen].bounds.size.height;
    return (_screenHeight - 402) / 4;
}

- (void) showPicker
{
    pickerView.hidden = NO;
    pickerBlocker.hidden = NO;
    CGRect _frame = pickerView.frame;
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        pickerView.frame = CGRectMake(_frame.origin.x, self.view.frame.size.height - _frame.size.height, _frame.size.width, _frame.size.height);
        pickerBlocker.alpha = 0.3;
    } completion:^(BOOL finished) {
        self.tap.enabled = YES;
    }];
}

- (void) hidePicker
{
    self.tap.enabled = NO;
    CGRect _frame = pickerView.frame;
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        pickerView.frame = CGRectMake(_frame.origin.x, self.view.frame.size.height, _frame.size.width, _frame.size.height);
        pickerBlocker.alpha = 0;
    } completion:^(BOOL finished) {
        pickerView.hidden = YES;
        pickerBlocker.hidden = YES;
        
        [tableView deselectRowAtIndexPath:tableView.indexPathForSelectedRow animated:NO];
    }];
}

- (void) togglePicker
{
    if (pickerView.hidden)
    {
        [self showPicker];
    }
    else
    {
        [self hidePicker];
    }
}

- (void) shareApp
{
    if ([UIActivityViewController class])
    {
        NSString *_text = @"Khay! The best way to learn about traffic conditions http://tari2ak.com";
        UIImage *_image = [UIImage imageNamed:@"screenshot.png"];
        WhatsAppMessage *_msg = [[WhatsAppMessage alloc] initWithMessage:_text forABID:nil];
        
        TACopyLinkActivity *_copyActivity = [[TACopyLinkActivity alloc] initWithURL:[NSURL URLWithString:@"http://tari2ak.com/download"]];
        JBWhatsAppActivity *_whatsAppActivity = [[JBWhatsAppActivity alloc] init];
        
        UIActivityViewController *avc = [[UIActivityViewController alloc]
                                         initWithActivityItems:@[_msg, _text, _image]
                                         applicationActivities:@[_whatsAppActivity, _copyActivity]];
        
        
        avc.completionHandler = ^(NSString *activityType, BOOL completed)
        {
            [self.tableView deselectRowAtIndexPath:tableView.indexPathForSelectedRow animated:NO];
            
            if (completed)
            {
                // Selected activity was performed
                NSString *_network = nil;
                NSString *_action = nil;
                
                if ([activityType isEqualToString:UIActivityTypeMail])
                {
                    _network = @"E-mail";
                    _action = @"Send";
                }
                else if ([activityType isEqualToString:UIActivityTypeMessage])
                {
                    _network = @"Message";
                    _action = @"Send";
                }
                else if ([activityType isEqualToString:UIActivityTypePostToFacebook])
                {
                    _network = @"Facebook";
                    _action = @"Post";
                }
                else if ([activityType isEqualToString:UIActivityTypePostToTwitter])
                {
                    _network = @"Twitter";
                    _action = @"Tweet";
                }
                else if ([activityType isEqualToString:@"es.sweetbits.WHATSAPP"])
                {
                    _network = @"WhatsApp";
                    _action = @"Send";
                }
                else if ([activityType isEqualToString:TAActivityTypeCopyLink])
                {
                    _network = @"Link";
                    _action = @"Copy";
                }
                
                if (_network)
                {
                    // Analytics Social Interaction
                    [tracker send:[[GAIDictionaryBuilder createSocialWithNetwork:_network
                                                                         action:_action
                                                                         target:nil] build]];
                }
            }
            else
            {
                if (activityType == NULL)
                {
                    // User dismissed the view controller without making a selection
                }
                else
                {
                    // Activity was not performed
                }
            }
        };
        
        avc.excludedActivityTypes = [NSArray arrayWithObjects:UIActivityTypeAssignToContact, nil];
        
        [self presentViewController:avc animated:YES completion:nil];
        
        // Analytics Event
        [self trackEventWithAction:@"Button Press" label:@"Share App"];
    }
}

- (void) showFacebookPage
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://profile/491338387651784"]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"fb://profile/491338387651784"]];
    }
    else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"http://facebook.com/tari2ak"]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://facebook.com/tari2ak"]];
    }
    
    // Analytics Event
    [self trackEventWithAction:@"Button Press" label:@"Facebook"];
}

- (void) showTwitterPage
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter://user?screen_name=tari2ak"]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter://user?screen_name=tari2ak"]];
    }
    else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"http://twitter.com/tari2ak"]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://twitter.com/tari2ak"]];
    }
    
    // Analytics Event
    [self trackEventWithAction:@"Button Press" label:@"Twitter"];
}

- (void) showAppOnStore
{
    NSString *appStoreLink = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=783043884";
//    NSNumber *appStoreID = @783043884;
    
    // Analytics Event
    [self trackEventWithAction:@"Button Press" label:@"Rate"];
    
//    if ([SKStoreProductViewController class])
//    {
//        SKStoreProductViewController *controller = [[SKStoreProductViewController alloc] init];
//        controller.delegate = self;
//        [controller loadProductWithParameters:@{ SKStoreProductParameterITunesItemIdentifier : appStoreID }
//                              completionBlock:NULL];
//        
//        [self presentViewController:controller animated:YES completion:nil];
//    }
//    else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:appStoreLink]])
//    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appStoreLink]];
//    }
}

@end
