//
//  TATextViewController.h
//  Tari2ak
//
//  Created by Rami Khawandi on 1/1/14.
//  Copyright (c) 2014 Rami Khawandi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TATextViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITextView *textView;

@property (nonatomic, strong) NSString *text;

@end
