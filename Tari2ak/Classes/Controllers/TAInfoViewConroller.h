//
//  TAInfoViewConroller.h
//  Tari2ak
//
//  Created by Rami Khawandi on 1/12/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <StoreKit/SKStoreProductViewController.h>
#import "GAITrackedViewController.h"

@interface TAInfoViewConroller : GAITrackedViewController <UIPickerViewDataSource, UIPickerViewDelegate, UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate, SKStoreProductViewControllerDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) IBOutlet UIGestureRecognizer *tap;

@property (nonatomic, strong) IBOutlet UIView *pickerBlocker;
@property (nonatomic, strong) IBOutlet UIPickerView *pickerView;

- (IBAction) tapOutsidePicker:(id)theSender;

@end
