//
//  TANavigationViewController.h
//  Tari2ak
//
//  Created by Rami Khawandi on 4/12/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TANavigationViewController : UINavigationController <UIAlertViewDelegate>

@end
