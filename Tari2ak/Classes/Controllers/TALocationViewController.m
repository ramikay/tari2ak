//
//  TALocationViewController.m
//  Tari2ak
//
//  Created by Rami Khawandi on 5/12/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import "TALocationViewController.h"
#import "TAConstants.h"
#import "UINavigationController+CustomTransitions.h"
#import "TAAppDelegate.h"
#import "ArabicConverter.h"

@interface TALocationViewController ()
{
    TAAppDelegate *appDelegate;
}

@end

@implementation TALocationViewController

//------------------------------------------
// View lifecycle
//------------------------------------------
#pragma mark - View lifecycle

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (TAAppDelegate *) [UIApplication sharedApplication].delegate;
    
    self.screenName = @"Location Lock";
    
    self.navigationItem.hidesBackButton = YES;
    
    // Title Label
    UILabel *_titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    _titleLabel.font = [UIFont fontWithName:@"BArabicStyle" size:38];
    _titleLabel.textColor = UIColor.whiteColor;
    _titleLabel.shadowColor = UIColor.blackColor;
    _titleLabel.text = [ArabicConverter convertArabic:@"طريقك"];
    _titleLabel.shadowOffset = CGSizeMake(1, 1);
    [_titleLabel sizeToFit];
    self.navigationItem.titleView = _titleLabel;
    
    BOOL shouldAllowUserToDismiss = YES;
    
    // Check if we should warn the user
    if (appDelegate.deniedFullLocationAccess)
    {
        // Check how many times the app has run without location access
        NSNumber *_disabledCount = [[NSUserDefaults standardUserDefaults] objectForKey:DISABLED_LOCATION_KEY];
        shouldAllowUserToDismiss = (_disabledCount.integerValue <= ALLOWED_USES_WITHOUT_LOCATION);
    }
    
    // Add the close button
    if (shouldAllowUserToDismiss)
    {
        UIButton *_closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        _closeButton.showsTouchWhenHighlighted = YES;
        [_closeButton setImage:[UIImage imageNamed:@"close-button.png"] forState:UIControlStateNormal];
        [_closeButton addTarget:self action:@selector(didTapCloseButton:) forControlEvents:UIControlEventTouchUpInside];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_closeButton];
    }
    
    CGFloat _tableHeight = [self tableView:self.tableView numberOfRowsInSection:0] * 44;
    CGFloat _tableY = self.tableView.frame.origin.y - (_tableHeight - 176);
    self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, _tableY, self.tableView.frame.size.width, _tableHeight);
}

//------------------------------------------
// Actions
//------------------------------------------
#pragma mark - Actions

- (void) didTapCloseButton:(id)theSender
{
    [self.navigationController popViewControllerUsingTransition:kCATransitionReveal
                                              transitionSubType:kCATransitionFromBottom
                                                       duration:0.3];
}

//------------------------------------------
// UIViewController methods
//------------------------------------------
#pragma mark - UIViewController methods

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL) shouldAutorotate
{
    return NO;
}

- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

//------------------------------------------
// UITableViewDataSource methods
//------------------------------------------
#pragma mark - UITableViewDataSource methods

- (NSInteger) tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)aSection
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(_iOS_8_0) && appDelegate.locationServicesEnabled && appDelegate.deniedFullLocationAccess)
        return 5;
    else
        return 4;
}

- (UITableViewCell *) tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)anIndexPath
{
    NSInteger _section = anIndexPath.section;
    NSInteger _row = anIndexPath.row;
    NSString *reuseIdentifier = @"LocationCell";
    UITableViewCell *_cell = [theTableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    if (!_cell)
    {
        _cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"LocationCell"];
        _cell.backgroundColor = [UIColor whiteColor];
        _cell.accessoryType = (_section == 1 && _row < 2) ? UITableViewCellAccessoryDisclosureIndicator : UITableViewCellAccessoryNone;
        
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if (!appDelegate.locationServicesEnabled)
    {
        switch (_row)
        {
            case 0:
                _cell.imageView.image = [UIImage imageNamed:@"icon-settings.png"];
                _cell.textLabel.text = @"Go to Settings";
                break;
                
            case 1:
                _cell.imageView.image = [UIImage imageNamed:@"icon-privacy.png"];
                _cell.textLabel.text = @"Go to Privacy";
                break;
                
            case 2:
                _cell.imageView.image = [UIImage imageNamed:@"icon-location.png"];
                _cell.textLabel.text = @"Go to Location Services";
                break;
                
            case 3:
                _cell.imageView.image = nil;
                _cell.textLabel.text = @"Turn on Location Services";
                break;
                
            default:
                break;
        }
    }
    else if (appDelegate.backgroundRefreshRequired && appDelegate.backgroundRefreshStatus != UIBackgroundRefreshStatusAvailable)
    {
        switch (_row)
        {
            case 0:
                _cell.imageView.image = [UIImage imageNamed:@"icon-settings.png"];
                _cell.textLabel.text = @"Go to Settings -> General";
                break;
                
            case 1:
                _cell.imageView.image = nil;
                _cell.textLabel.text = @"Go to Background App Refresh";
                break;
                
            case 2:
                _cell.imageView.image = nil;
                _cell.textLabel.text = @"Turn on Background App Refresh";
                break;
                
            case 3:
                _cell.imageView.image = [UIImage imageNamed:@"Icon-Small.png"];
                _cell.textLabel.text = @"Allow access for Tari'ak";
                break;
                
            default:
                break;
        }
    }
    else
    {
        switch (_row)
        {
            case 0:
                _cell.imageView.image = [UIImage imageNamed:@"icon-settings.png"];
                _cell.textLabel.text = @"Go to Settings";
                break;
                
            case 1:
                _cell.imageView.image = [UIImage imageNamed:@"icon-privacy.png"];
                _cell.textLabel.text = @"Go to Privacy";
                break;
                
            case 2:
                _cell.imageView.image = [UIImage imageNamed:@"icon-location.png"];
                _cell.textLabel.text = @"Go to Location Services";
                break;
                
            case 3:
                _cell.imageView.image = [UIImage imageNamed:@"Icon-Small.png"];
                _cell.textLabel.text = (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(_iOS_8_0)) ? @"Select Tari'ak" : @"Allow access for Tari'ak";
                break;
                
            case 4:
                _cell.imageView.image = [UIImage imageNamed:@"icon-always.png"];
                _cell.textLabel.text = @"Choose Always";
                break;
                
            default:
                break;
        }
    }
    
    return _cell;
}

//------------------------------------------
// UITableViewDelegate methods
//------------------------------------------
#pragma mark - UITableViewDelegate methods

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(_iOS_8_0) && appDelegate.locationServicesEnabled && appDelegate.deniedFullLocationAccess)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}

@end
