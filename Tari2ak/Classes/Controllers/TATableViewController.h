//
//  TATableViewController.h
//  Tari2ak
//
//  Created by Rami Khawandi on 181/6/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TADataRetriever.h"

@class ODRefreshControl;

@interface TATableViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate>
{
    TADataRetriever *dataRetriever;
    ODRefreshControl *refreshControl;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;

- (IBAction) popToMapViewController:(id)theSender;
- (IBAction) didTapInfoButton:(id)theSender;

@end
