//
//  TANavigationViewController.m
//  ;
//
//  Created by Rami Khawandi on 4/12/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import "TANavigationViewController.h"
#import "TAMapViewController.h"
#import "TALocationViewController.h"
#import "TANavigationTransitionSegue.h"
#import "TADataRetriever.h"
#import "TAAppDelegate.h"
#import "TAConstants.h"
#import "UIApplication+Utils.h"

@interface TANavigationViewController ()
{
    TAAppDelegate *appDelegate;
    TADataRetriever *dataRetriever;
}
@end

@implementation TANavigationViewController

//------------------------------------------
// View lifecycle
//------------------------------------------
#pragma mark - View lifecycle

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (TAAppDelegate *) [[UIApplication sharedApplication] delegate];
    dataRetriever = [TADataRetriever sharedInstance];
    
    NSNotificationCenter *_nc = [NSNotificationCenter defaultCenter];
    
    [_nc addObserver:self selector:@selector(maybeLockTheApp) name:UIApplicationDidBecomeActiveNotification object:nil];
    [_nc addObserver:self selector:@selector(maybeLockTheApp) name:TALocationStatusChanged object:nil];
    [_nc addObserver:self selector:@selector(updateDidFail:) name:TADataRetrieverUpdateFailed object:nil];
}

- (void) viewDidAppear:(BOOL)isAnimated
{
    [super viewDidAppear:isAnimated];
    
    [self maybeLockTheApp];
}

- (void) viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewDidUnload];
}

//------------------------------------------
// UIViewController methods
//------------------------------------------
#pragma mark - UIViewController methods

- (void) prepareForSegue:(UIStoryboardSegue *)theSegue sender:(id)theSender
{
    if ([theSegue isKindOfClass:[TANavigationTransitionSegue class]])
    {
        TANavigationTransitionSegue *_segue = (TANavigationTransitionSegue *)theSegue;
        
        _segue.transitionType = kCATransitionMoveIn;
        _segue.transitionSubtype = kCATransitionFromTop;
        _segue.duration = 0.3;
        _segue.action = TANavigationPush;
    }
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return [self.viewControllers.lastObject shouldAutorotateToInterfaceOrientation:toInterfaceOrientation];
}

-(BOOL)shouldAutorotate
{
    return [self.viewControllers.lastObject shouldAutorotate];
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return [self.viewControllers.lastObject supportedInterfaceOrientations];
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return [self.viewControllers.lastObject preferredInterfaceOrientationForPresentation];
}

//------------------------------------------
// Actions
//------------------------------------------
#pragma mark - Actions

- (void) maybeLockTheApp
{
#ifndef DEBUG
    if (!appDelegate.locationEnabled && appDelegate.authorizationStatus != kCLAuthorizationStatusNotDetermined)
    {
        if (![self.visibleViewController isKindOfClass:[TALocationViewController class]])
        {
            [self performSegueWithIdentifier:@"LockTheApp" sender:self];
        }
    }
    else if ([self.visibleViewController isKindOfClass:[TALocationViewController class]])
    {
        [self popViewControllerAnimated:NO];
    }
#endif
}

- (void) updateDidFail:(NSNotification *)theNotification
{
    
    [[UIApplication sharedApplication] dismissAllAlertViews];
    
    NSError *_error = [theNotification object];
    NSNumber *_errorCode = [NSNumber numberWithInt:_error.code];
    NSArray *_connectionErrorCodes = [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:NSURLErrorUnknown],
                                                                      [NSNumber numberWithInt:NSURLErrorTimedOut],
                                                                      [NSNumber numberWithInt:NSURLErrorCannotFindHost],
                                                                      [NSNumber numberWithInt:NSURLErrorCannotConnectToHost],
                                                                      [NSNumber numberWithInt:NSURLErrorNetworkConnectionLost], nil];
    
    NSString *_errorMessage;
    NSString *_cancelButtonTitle;
    NSString *_otherButtonTitle;
    
    if (_error.code == NSURLErrorNotConnectedToInternet)
    {
        _errorMessage = TALocalizedString(@"No network connection");
        _cancelButtonTitle = TALocalizedString(@"OK");
        _otherButtonTitle = nil;
    }
    else if ([_connectionErrorCodes containsObject:_errorCode])
    {
        _errorMessage = TALocalizedString(@"Could not reach server");
        _cancelButtonTitle = TALocalizedString(@"Cancel");
        _otherButtonTitle = TALocalizedString(@"Try again");
    }
    else
    {
        _errorMessage = TALocalizedString(@"An error occurred");
        _cancelButtonTitle = TALocalizedString(@"Cancel");
        _otherButtonTitle = TALocalizedString(@"Try again");
    }
    
    UIAlertView *_alertView = [[UIAlertView alloc] initWithTitle:_errorMessage
                                                         message:nil
                                                        delegate:self
                                               cancelButtonTitle:_cancelButtonTitle
                                               otherButtonTitles:_otherButtonTitle, nil];
    
    [_alertView show];
    
#if DEBUG
    
    NSLog(@"%@", _error.description);
    
#endif
}

//------------------------------------------
// UIAlertViewDelegate methods
//------------------------------------------
#pragma mark - UIAlertViewDelegate methods

- (void) alertView:(UIAlertView *)theAlertView didDismissWithButtonIndex:(NSInteger)theButtonIndex
{
    if (theButtonIndex == 1)
    {
        [dataRetriever update];
    }
}

@end
