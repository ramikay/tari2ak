//
//  TAMapViewController.m
//  Tari2ak
//
//  Created by Rami on 27/3/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import "TAMapViewController.h"
#import "TADataReporter.h"
#import "TADataRetriever.h"
#import "TAStreet.h"
#import <QuartzCore/QuartzCore.h>
#import "MKMapView+Utils.h"
#import "TAConstants.h"
#import "UILabel+Arabic.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "TAStreetAnnotation.h"
#import "ZSPinAnnotation.h"
#import "TAAppDelegate.h"
#import "UIViewController+Utils.h"
#import "ArabicConverter.h"
#import "TAPolylineRenderer.h"
#import "UIView+Utils.h"
#import "UIImage+Utils.h"
#import "UIApplication+Utils.h"

#define UDPATE_ERROR_ALERT_TAG          192
#define LOCATION_FAILED_ALERT_TAG       271

#define ACTION_SHEET_TAG                933
#define SCREENSHOT_SHEET_TAG            289

#define MINIMUM_ANIMATION_ZOOM_LEVEL    12

@interface TAMapViewController ()
{
    NSArray *lineDashPattern;
    
    double currentZoomLevel;
    BOOL isMapMoving;
    BOOL cancelToolbarAnimation;
    
    TAAppDelegate *appDelegate;
    TALocaleManager *localeManager;
    
    id tracker;
    NSDate *lastOrientationChange;
    
    NSTimer *animationTimer;
    NSTimer *scaleTimer;
    NSTimer *toolbarTimer;
    
    NSMutableDictionary *allPolylineRenderers;
    NSMutableDictionary *visiblePolylineRenderers;
    
}

- (void) didTapMap:(UITapGestureRecognizer *)gesture;
- (void) toggleToolbars;
- (void) didTapActionButton;
- (void) chooseScreenshotSize;
- (void) didTapInfoButton;

- (void) shareFullScreenshot;
- (void) shareSquareScreenshot;

- (void) trackOrientation;
- (void) trackEventWithAction:(NSString *)theAction label:(NSString *)theLabel value:(NSNumber *)theValue;

- (void) reloadLocale;
- (void) refreshOverlays;
- (void) clearAnnotations;
- (void) drawPathForStreet:(TAStreet *)theStreet;

- (UIColor *) colorForStreet:(TAStreet *)theStreet;
- (NSArray *) lineDashPatternForStreet:(TAStreet *)theStreet;
- (NSInteger) frameIntervalForStreet:(TAStreet *)theStreet;
- (CGFloat) streetWidthForZoomLevel:(NSUInteger)theZoomLevel;

- (void) startPolylineAnimation;

- (void) initBarButtonItems;
- (void) clearBarButtonItems;
@end

@implementation TAMapViewController

@synthesize didLoadMap;
@synthesize mapView, watermark;
@synthesize loadingContainer,loadingLabel,activityIndicator;
@synthesize searchBar, refreshButton, locationButton, typeButton;

//------------------------------------------
// View lifecycle
//------------------------------------------
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (TAAppDelegate *) [[UIApplication sharedApplication] delegate];
    localeManager = [TALocaleManager sharedInstance];
    tracker = [[GAI sharedInstance] defaultTracker];
    
    self.screenName = @"Map";
    
    didLoadMap = NO;
    
    CALayer *_viewLayer = self.loadingContainer.layer;
    
    _viewLayer.cornerRadius = 10;
    _viewLayer.masksToBounds = YES;
    
    self.refreshButton.layer.masksToBounds = YES;
    self.locationButton.layer.masksToBounds = YES;
    self.typeButton.layer.masksToBounds = YES;
    
    self.watermark.shadowOffset = CGSizeMake(-1, 1);
    
    self.refreshButton.layer.cornerRadius = 25;
    self.locationButton.layer.cornerRadius = 25;
    self.typeButton.layer.cornerRadius = 25;
    
    [self initBarButtonItems];
    
    // Searchbar
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.searchBarWidth, 44)];
    searchBar.searchBarStyle = UISearchBarStyleMinimal;
    searchBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    searchBar.delegate = self;
    searchBar.tintColor = [UIColor whiteColor];
    [searchBar sizeToFit];
    
    // Map Gestures
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapMap:)];
    tapGesture.numberOfTapsRequired = 1;
    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:nil action:nil];
    doubleTapGesture.numberOfTapsRequired = 2;
    
    [mapView addGestureRecognizer:tapGesture];
    [mapView addGestureRecognizer:doubleTapGesture];
    
    [tapGesture requireGestureRecognizerToFail:doubleTapGesture];

    // Default map settings
    if ([mapView respondsToSelector:@selector(setShowsCompass:)])
        mapView.showsCompass = YES;
    if ([mapView respondsToSelector:@selector(setShowsTraffic:)])
        mapView.showsTraffic = NO;
    if ([mapView respondsToSelector:@selector(setShowsScale:)])
        mapView.showsScale = YES;
    
    // Set initial map region (just in case)
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = 33.863574;
    zoomLocation.longitude= 35.531158;
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 10*METERS_PER_MILE, 10*METERS_PER_MILE);
    [mapView setRegion:viewRegion animated:NO];
    
    // Restore map type state
//    MKMapType _initialMapType = [[NSUserDefaults standardUserDefaults] integerForKey:INITIAL_MAP_TYPE_KEY];
//    mapView.mapType = _initialMapType;
    
    if (mapView.mapType != MKMapTypeStandard)
    {
        typeButton.selected = YES;
        watermark.alpha = 1.0;
        watermark.textColor = [UIColor whiteColor];
        watermark.shadowColor = [UIColor blackColor];
        
        if ([mapView respondsToSelector:@selector(setTintColor:)]) { mapView.tintColor = [UIColor whiteColor]; }
    }
    
    // Analytics Event
//    NSNumber *_satelliteEnabled = [NSNumber numberWithBool:(_initialMapType != MKMapTypeStandard)];
//    [self trackEventWithAction:@"Initial Setting" label:@"Satellite" value:_satelliteEnabled];
    
    MKUserTrackingMode _mapTrackingMode = [[NSUserDefaults standardUserDefaults] integerForKey:INITIAL_MAP_MODE_KEY];
    
    // Restore map camera
    NSUserDefaults *_defaults = [NSUserDefaults standardUserDefaults];
    double _lat = [_defaults doubleForKey:INITIAL_MAP_CAMERA_LATITUDE];
    double _lon = [_defaults doubleForKey:INITIAL_MAP_CAMERA_LONGITUDE];
    double _heading = [_defaults doubleForKey:INITIAL_MAP_CAMERA_HEADING];
    double _altitude = [_defaults doubleForKey:INITIAL_MAP_CAMERA_ALTITUDE];
    
    if (_lat != 0 && _lon != 0)
    {
        MKMapCamera *_cam = [[MKMapCamera alloc] init];
        _cam.centerCoordinate = CLLocationCoordinate2DMake(_lat, _lon);
        _cam.heading = _heading;
        _cam.pitch = 0;
        _cam.altitude = _altitude;
        
        [mapView setCamera:_cam animated:NO];
    }
    
    // Search container background
    UIToolbar *bluredView = [[UIToolbar alloc] init];
    bluredView.barStyle = UIBarStyleBlackTranslucent;
    [bluredView setFrame:self.searchContainer.bounds];
    bluredView.autoresizingMask = self.searchContainer.autoresizingMask;

    [self.searchContainer insertSubview:bluredView atIndex:0];
    
    // Analytics Event
    NSNumber *_trackingEnabled = [NSNumber numberWithBool:(_mapTrackingMode != MKUserTrackingModeNone)];
    [self trackEventWithAction:@"Initial Setting" label:@"Tracking" value:_trackingEnabled];
    
    watermark.font =  [UIFont fontWithName:@"BArabicStyle" size:26];
    [watermark setArabicText:@"طريقك"];
    
    dataRetriever = [TADataRetriever sharedInstance];
    
    allPolylineRenderers = [[NSMutableDictionary alloc] init];
    visiblePolylineRenderers = [[NSMutableDictionary alloc] init];
    
    NSNotificationCenter *_nc = [NSNotificationCenter defaultCenter];
    
    [_nc addObserver:self selector:@selector(didStartUpdate:) name:TADataRetrieverUpdateBegan object:nil];
    [_nc addObserver:self selector:@selector(didUpdateStreets:) name:TADataRetrieverStreetsUpdateComplete object:nil];
    [_nc addObserver:self selector:@selector(didReceiveError:) name:TADataRetrieverUpdateFailed object:nil];
    [_nc addObserver:self selector:@selector(didChangeLocationStatus:) name:TALocationStatusChanged object:nil];
    [_nc addObserver:self selector:@selector(reloadLocale) name:TALocaleChanged object:nil];
    [_nc addObserver:self selector:@selector(appWillResignActive) name:UIApplicationWillResignActiveNotification object:nil];
    [_nc addObserver:self selector:@selector(appDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) viewWillAppear:(BOOL)isAnimated
{
    [super viewWillAppear:isAnimated];
    
    lastOrientationChange = [NSDate date];
    
    [self.navigationController.navigationBar addSubview:searchBar];
    
    searchBar.center = CGPointMake(self.navigationController.navigationBar.center.x, searchBar.center.y);
    
    [self reloadLocale];
    
    [[UIApplication sharedApplication] setStatusBarHidden:self.toolbar.hidden];
}

- (void)viewDidAppear:(BOOL)isAnimated
{
    [super viewDidAppear:isAnimated];
    
    // Dirty hack for setting the text bar color.
    for(UIView *subView in searchBar.subviews)
    {
        for(UIView *subsubView in subView.subviews)
        {
            if([subsubView isKindOfClass:UITextField.class])
                [(UITextField*)subsubView setTextColor:[UIColor whiteColor]];
        }
    }
    
    self.locationButton.enabled = appDelegate.locationEnabled;
    
    if (dataRetriever.updateInProgress)
    {
        loadingContainer.hidden = NO;
    }
    else
    {
        loadingContainer.hidden = YES;
        [self refreshOverlays];
    }
}

- (void) viewWillDisappear:(BOOL)isAnimated
{
    [super viewWillDisappear:isAnimated];
    
    [searchBar resignFirstResponder];
    [searchBar removeFromSuperview];
    
    [self stopPolylineAnimation];
    
    [self clearAnnotations];
    
    // Analytics User Timing
    [self trackOrientation];
    
}

- (void) appDidBecomeActive
{
    lastOrientationChange = [NSDate date];
    
    if (mapView.zoomLevel >= MINIMUM_ANIMATION_ZOOM_LEVEL)
    {
        [self startPolylineAnimation];
    }
}

- (void) appWillResignActive
{
    [self stopPolylineAnimation];
    
    [self clearAnnotations];
    
    // Analytics User Timing
    [self trackOrientation];
}

//------------------------------------------
// UIViewController methods
//------------------------------------------
#pragma mark - UIViewController methods

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation))
    {
        if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
        {
            searchBar.center = CGPointMake(self.navigationController.navigationBar.center.x, searchBar.center.y-6);
        }
        
        [UIApplication sharedApplication].idleTimerDisabled = YES;
        
        // Analytics User Timing
        if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
        {
            [self trackOrientation];
        }
    }
    else
    {
        searchBar.center = CGPointMake(self.navigationController.navigationBar.center.x, searchBar.center.y+6);
        
        [UIApplication sharedApplication].idleTimerDisabled = NO;
        
        // Analytics User Timing
        if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
        {
            [self trackOrientation];
        }
    }
}

//------------------------------------------
// Actions
//------------------------------------------
#pragma mark - Actions

- (IBAction) refreshAction:(id)theSender
{
    [dataRetriever update];
}

- (IBAction) locationAction:(id)theSender
{
    MKUserTrackingMode _mode = mapView.userTrackingMode;
    
    if (_mode == MKUserTrackingModeNone)
    {
        [mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    }
    else if (_mode == MKUserTrackingModeFollow)
    {
        [mapView setUserTrackingMode:MKUserTrackingModeFollowWithHeading animated:YES];
    }
    else
    {
        [mapView setUserTrackingMode:MKUserTrackingModeNone animated:YES];
    }
    
    // Analytics Event
    NSNumber *_trackingEnabled = [NSNumber numberWithBool:(mapView.userTrackingMode != MKUserTrackingModeNone)];
    [self trackEventWithAction:@"Button Press" label:@"Tracking" value:_trackingEnabled];
}

- (IBAction) typeAction:(id)theSender
{
    if (mapView.mapType == MKMapTypeHybrid)
    {
        mapView.mapType = MKMapTypeStandard;
        watermark.alpha = 0.4;
        watermark.textColor = [UIColor blackColor];
        watermark.shadowColor = [UIColor clearColor];
        
        if ([mapView respondsToSelector:@selector(setTintColor:)]) { mapView.tintColor = RGB(0,122,255); }
    }
    else
    {
        mapView.mapType = MKMapTypeHybrid;
        watermark.alpha = 1.0;
        watermark.textColor = [UIColor whiteColor];
        watermark.shadowColor = [UIColor blackColor];
        
        if ([mapView respondsToSelector:@selector(setTintColor:)]) { mapView.tintColor = [UIColor whiteColor]; }
    }
    
    // Analytics Event
    NSNumber *_satelliteEnabled = [NSNumber numberWithBool:(mapView.mapType != MKMapTypeStandard)];
    [self trackEventWithAction:@"Button Press" label:@"Satellite" value:_satelliteEnabled];
    
    typeButton.selected = !typeButton.selected;
    
    [[NSUserDefaults standardUserDefaults] setInteger:mapView.mapType forKey:INITIAL_MAP_TYPE_KEY];
}

- (void) didTapInfoButton
{
    [self.navigationController performSegueWithIdentifier:@"GoToInfo" sender:self];
}

- (void) didTapActionButton
{
    UIActionSheet *shareSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                            delegate:self
                                                   cancelButtonTitle:TALocalizedString(@"Cancel")
                                              destructiveButtonTitle:nil
                                                   otherButtonTitles:TALocalizedString(@"Take Screenshot"), TALocalizedString(@"Share Location"), nil];
    
    shareSheet.tag = ACTION_SHEET_TAG;
    [shareSheet showInView:self.view];
}

- (void) didStartUpdate:(NSNotification *)theNotification
{
    loadingContainer.hidden = NO;
}

- (void) didUpdateStreets:(NSNotification *)theNotification
{
    loadingContainer.hidden = YES;
    
    [self refreshOverlays];
}

- (void) didReceiveError:(NSNotification *)theNotification
{
    loadingContainer.hidden = YES;
}

- (void) didChangeLocationStatus:(NSNotification *)theNotification
{
    self.locationButton.enabled = appDelegate.locationEnabled;
}

//------------------------------------------
// Public methods
//------------------------------------------
#pragma mark - Public methods

- (void) beginPolylineAnimationAfter:(NSTimeInterval)delay
{
    if (delay > 0.000)
    {
        animationTimer = [NSTimer scheduledTimerWithTimeInterval:delay target:self selector:@selector(startPolylineAnimation) userInfo:nil repeats:NO];
    }
    else
    {
        [self startPolylineAnimation];
    }
}

- (void) startPolylineAnimation
{
    @synchronized(visiblePolylineRenderers)
    {
        [visiblePolylineRenderers.allValues makeObjectsPerformSelector:@selector(startAnimating)];
    }
}

- (void) stopPolylineAnimation
{
    [animationTimer invalidate];
    animationTimer = nil;
    
    @synchronized(allPolylineRenderers)
    {
        [allPolylineRenderers.allValues makeObjectsPerformSelector:@selector(stopAnimating)];
    }
}

- (void) showAnnotationForStreet:(TAStreet *)aStreet title:(NSString *)theTitle subtitle:(NSString *)theSubtitle
{
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(aStreet.midPoint.coordinate, mapView.region.span.latitudeDelta, mapView.region.span.longitudeDelta);
    
    TAStreetAnnotation *_annotation = [[TAStreetAnnotation alloc] initWithCoordinate:aStreet.midPoint.coordinate
                                                                               title:theTitle
                                                                            subtitle:theSubtitle
                                                                               color:[self colorForStreet:aStreet]];
    if (!didLoadMap)
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [mapView setRegion:viewRegion animated:YES];
            [mapView addAnnotation:_annotation];
        });
    }
    else
    {
        [self clearAnnotations];
        
        [mapView setRegion:viewRegion animated:YES];
        [mapView addAnnotation:_annotation];
    }
    
}

//------------------------------------------
// Private methods
//------------------------------------------
#pragma mark - Private methods

- (void) showScale
{
    if ([mapView respondsToSelector:@selector(setShowsScale:)])
    {
        [scaleTimer invalidate];
        scaleTimer = nil;
        
        self.mapView.showsScale = YES;
    }
}

- (void) hideScale
{
    if ([mapView respondsToSelector:@selector(setShowsScale:)])
    {
        self.mapView.showsScale = NO;
    }
}

- (void) hideScaleAfter:(NSTimeInterval)duration
{
    if ([mapView respondsToSelector:@selector(setShowsScale:)])
    {
        if (duration > 0.000)
        {
            scaleTimer = [NSTimer scheduledTimerWithTimeInterval:duration target:self selector:@selector(hideScale) userInfo:nil repeats:NO];
        }
        else
        {
            [self hideScale];
        }
    }
}

- (void) initBarButtonItems
{
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
    [leftButton addTarget:self action:@selector(didTapInfoButton) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(didTapActionButton)];
}

- (void) clearBarButtonItems
{
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = nil;
}

- (void) trackOrientation
{
    NSTimeInterval _orientationTime = [[NSDate date] timeIntervalSinceDate:lastOrientationChange];
    
    // Track it only if the orientation time is larger than 1 to avoid unintentional rotation
    if (_orientationTime > 1)
    {
        NSInteger _durationInteger = round(_orientationTime*1000);
        NSNumber *_duration = [NSNumber numberWithDouble:_durationInteger];    // In milliseconds
        NSString *_orientation = (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) ? @"Landscape" : @"Portrait";
        
        [tracker send:[[GAIDictionaryBuilder createTimingWithCategory:@"Map"
                                                             interval:_duration
                                                                 name:@"Orientation"
                                                                label:_orientation] build]];
    }
    
    lastOrientationChange = [NSDate date];
}

- (void) trackEventWithAction:(NSString *)theAction label:(NSString *)theLabel value:(NSNumber *)theValue
{
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Map"
                                                          action:theAction
                                                           label:theLabel
                                                           value:theValue] build]];
}

- (void) reloadLocale
{
    BOOL _isArabic = [localeManager.locale isEqualToString:LOCALE_ARABIC];
    
    CGFloat _loadingLabelSize = (_isArabic) ? 32 : 18;
    NSString *_fontName = (_isArabic) ? @"BArabicStyle" : @"HelveticaNeue-Bold";

    [loadingLabel setArabicText:TALocalizedString(@"Loading")];
    loadingLabel.font = [UIFont fontWithName:_fontName size:_loadingLabelSize];
    
}

- (CGFloat) searchBarWidth
{
    return self.view.width - 140;
}

- (void) refreshOverlays
{
    [self stopPolylineAnimation];
    
    [self.mapView removeOverlays:self.mapView.overlays];
    
    @synchronized(visiblePolylineRenderers)
    {
        [visiblePolylineRenderers removeAllObjects];
    }
    
    @synchronized(allPolylineRenderers)
    {
        [allPolylineRenderers removeAllObjects];
    }
    
    NSArray *_streetObjects = [dataRetriever.streets allValues];
    
    for (TAStreet *_street in _streetObjects)
    {
        [self drawPathForStreet:_street];
    }
}

- (void) clearAnnotations
{
    [mapView removeAnnotations:mapView.annotations];
}

- (void) drawPathForStreet:(TAStreet *)theStreet
{
    NSArray *_path = theStreet.nodes;
    NSString *_id = theStreet.identifier;
    
    NSInteger _numberOfNodes = _path.count;
        
    CLLocationCoordinate2D _coordinates[_numberOfNodes];
    
    for (int _i = 0; _i < _numberOfNodes; _i++)
    {
        CLLocation *_location = [_path objectAtIndex:_i];
        CLLocationCoordinate2D _coordinate = _location.coordinate;
        
        _coordinates[_i] = _coordinate;
    }
    
    MKPolyline *polyLine = [MKPolyline polylineWithCoordinates:_coordinates count:_numberOfNodes];
    polyLine.title = _id;
    
    if ([self.mapView respondsToSelector:@selector(addOverlay:level:)])
    {
        [self.mapView addOverlay:polyLine level:MKOverlayLevelAboveRoads];
    }
    else
    {
        [self.mapView addOverlay:polyLine];
    }
}

- (UIColor *) colorForStreet:(TAStreet *)theStreet
{
    if (theStreet.condition == TATrafficConditionVeryHeavy)
    {
        return RGB(200,8,13);
    }
    else if (theStreet.condition == TATrafficConditionHeavy)
    {
        return RGB(255,22,28);
    }
    else if (theStreet.condition == TATrafficConditionMedium)
    {
        return RGB(255,150,54);
    }
    else
    {
        return RGB(78,226,96);
    }
}

- (NSArray *) lineDashPatternForStreet:(TAStreet *)theStreet
{
    if (theStreet.condition == TATrafficConditionHeavy || theStreet.condition == TATrafficConditionVeryHeavy)
    {
        return @[@25,@15];
    }
    else if (theStreet.condition == TATrafficConditionMedium)
    {
        return @[@25,@25];
    }
    else
    {
        return @[@25,@45];
    }
}

- (NSInteger) frameIntervalForStreet:(TAStreet *)theStreet
{
    if (theStreet.condition == TATrafficConditionHeavy)
    {
        return 40;
    }
    else if (theStreet.condition == TATrafficConditionMedium)
    {
        return 20;
    }
    else
    {
        return 1;
    }
}

- (CGFloat) streetWidthForZoomLevel:(NSUInteger)theZoomLevel
{
    if (theZoomLevel >= 10)
    {
        return 4.0;
    }
    else
    {
        return 2.0;
    }
}

- (void) didTapMap:(UITapGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateEnded)
    {
        toolbarTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(toggleToolbars) userInfo:nil repeats:NO];
    }
}

- (void) toggleToolbars
{
    [toolbarTimer invalidate];
    toolbarTimer = nil;
    
    if (!cancelToolbarAnimation)
    {
        [self.navigationController setNavigationBarHidden:!self.navigationController.navigationBarHidden animated:YES];
        
        UIApplication *app = [UIApplication sharedApplication];
        [app setStatusBarHidden:!app.statusBarHidden withAnimation:UIStatusBarAnimationSlide];
        
        BOOL shouldHide = !self.toolbar.hidden;
        CGFloat newToolbarY = (self.toolbar.hidden) ? self.view.height - self.toolbar.height : self.view.height;
        CGFloat newWatermarkY = (self.toolbar.hidden) ? self.watermark.frame.origin.y - self.toolbar.height : self.watermark.frame.origin.y + self.toolbar.height;
        
        BOOL showScale = ([mapView respondsToSelector:@selector(setShowsScale:)]) ? [mapView showsScale] : NO;
        BOOL showCompass = ([mapView respondsToSelector:@selector(setShowsCompass:)]) ? [mapView showsCompass] : NO;
        
        [UIView animateWithDuration:0.2
                         animations:^{
                             
                             if ([mapView respondsToSelector:@selector(setShowsScale:)])
                                 mapView.showsScale = NO;
                             
                             if ([mapView respondsToSelector:@selector(setShowsCompass:)])
                                 mapView.showsCompass = NO;
                             
                             self.toolbar.hidden = NO;
                             self.toolbar.frame = CGRectMake(self.toolbar.frame.origin.x, newToolbarY, self.toolbar.frame.size.width, self.toolbar.frame.size.height);
                             self.watermark.frame = CGRectMake(self.watermark.frame.origin.x, newWatermarkY, self.watermark.frame.size.width, self.watermark.frame.size.height);
                             
                         } completion:^(BOOL finished) {
                             
                             self.toolbar.hidden = shouldHide;
                             
                             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                 mapView.showsScale = showScale;
                                 mapView.showsCompass = showCompass;
                             });
                         }];
    }
    
    cancelToolbarAnimation = NO;
}

- (void) shareFullScreenshot
{
    self.loadingContainer.hidden = NO;
    
    if ([mapView respondsToSelector:@selector(setShowsCompass:)])
        self.mapView.showsCompass = NO;
    if ([mapView respondsToSelector:@selector(setShowsScale:)])
        self.mapView.showsScale = NO;
    
    UIImage *mapImage = [mapView captureSnapshot];
    
    if ([mapView respondsToSelector:@selector(setShowsCompass:)])
        self.mapView.showsCompass = YES;
    
    UIImage *watermarkImage = [UIImage imageNamed:@"Logo"];
    
    CGSize screenshotSize = CGSizeMake(mapImage.size.width * UIScreen.mainScreen.scale, mapImage.size.height * UIScreen.mainScreen.scale);
    
    UIGraphicsBeginImageContext(screenshotSize);
    [mapImage drawInRect:CGRectMake(0, 0, screenshotSize.width, screenshotSize.height)];
    [watermarkImage drawInRect:CGRectMake(screenshotSize.width - watermarkImage.size.width - 10, screenshotSize.height - watermarkImage.size.height - 10, watermarkImage.size.width, watermarkImage.size.height)];
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if ([UIActivityViewController class])
    {
        UIActivityViewController *avc = [[UIActivityViewController alloc]
                                         initWithActivityItems:@[result]
                                         applicationActivities:nil];
        
        avc.completionHandler = ^(NSString *activityType, BOOL completed)
        {
            self.loadingContainer.hidden = YES;
        };
        
        avc.excludedActivityTypes = [NSArray arrayWithObjects:UIActivityTypeAssignToContact, nil];
        
        [self presentViewController:avc animated:YES completion:^{
            self.loadingContainer.hidden = YES;
        }];
    }
}

- (void) shareSquareScreenshot
{
    self.loadingContainer.hidden = NO;
    
    if ([mapView respondsToSelector:@selector(setShowsCompass:)])
        self.mapView.showsCompass = NO;
    if ([mapView respondsToSelector:@selector(setShowsScale:)])
        self.mapView.showsScale = NO;
    
    UIImage *mapImage = [mapView captureSnapshot];
    
    if ([mapView respondsToSelector:@selector(setShowsCompass:)])
        self.mapView.showsCompass = YES;
    
    double minDimension = ((mapImage.size.width < mapImage.size.height) ? mapImage.size.width : mapImage.size.height) * UIScreen.mainScreen.scale;
    UIImage *croppedScreenshot = [mapImage imageByCroppingToSize:CGSizeMake(minDimension, minDimension)];
    
    UIImage *watermarkImage = [UIImage imageNamed:@"Logo"];
    
    UIGraphicsBeginImageContext(croppedScreenshot.size);
    [croppedScreenshot drawInRect:CGRectMake(0, 0, croppedScreenshot.size.width, croppedScreenshot.size.height)];
    [watermarkImage drawInRect:CGRectMake(croppedScreenshot.size.width - watermarkImage.size.width - 10, croppedScreenshot.size.height - watermarkImage.size.height - 10, watermarkImage.size.width, watermarkImage.size.height)];
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if ([UIActivityViewController class])
    {
        UIActivityViewController *avc = [[UIActivityViewController alloc]
                                         initWithActivityItems:@[result]
                                         applicationActivities:nil];
        
        avc.completionHandler = ^(NSString *activityType, BOOL completed)
        {
            self.loadingContainer.hidden = YES;
        };
        
        avc.excludedActivityTypes = [NSArray arrayWithObjects:UIActivityTypeAssignToContact, nil];
        
        [self presentViewController:avc animated:YES completion:^{
            self.loadingContainer.hidden = YES;
        }];
    }
}

- (void) shareLocation
{
    CLLocation *userLocation = mapView.userLocation.location;
    
    if (!userLocation)
    {
        [[UIApplication sharedApplication] dismissAllAlertViews];
        
        [[[UIAlertView alloc] initWithTitle:TALocalizedString(@"Could not locate you")
                                    message:nil
                                   delegate:nil
                          cancelButtonTitle:TALocalizedString(@"OK")
                          otherButtonTitles:nil] show];
    }
    else
    {
        self.loadingContainer.hidden = NO;
        
        NSString *mapURLString = [NSString stringWithFormat:TALocalizedString(@"I'm here: http://tari2ak.com/location?ll=%f,%f"),userLocation.coordinate.latitude,userLocation.coordinate.longitude];
        
        NSArray *itemsToShare = [[NSArray alloc] initWithObjects: mapURLString, nil] ;
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
        activityVC.excludedActivityTypes = @[UIActivityTypePrint,
                                             UIActivityTypeAssignToContact,
                                             UIActivityTypeSaveToCameraRoll];
        
        activityVC.completionHandler = ^(NSString *activityType, BOOL completed)
        {
            self.loadingContainer.hidden = YES;
        };
        
        [self presentViewController:activityVC animated:YES completion:^{
            self.loadingContainer.hidden = NO;
        }];
    }
}

- (void) setSearchVisible:(BOOL)isVisible
{
    self.searchContainer.hidden = !isVisible;
}

- (void) chooseScreenshotSize
{
    UIActionSheet *shareSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                            delegate:self
                                                   cancelButtonTitle:TALocalizedString(@"Cancel")
                                              destructiveButtonTitle:nil
                                                   otherButtonTitles:TALocalizedString(@"Square (1:1)"), TALocalizedString(@"Full (16:9)"), nil];
    
    shareSheet.tag = SCREENSHOT_SHEET_TAG;
    [shareSheet showInView:self.view];
}

//------------------------------------------
// MKMapViewDelegate methods
//------------------------------------------
#pragma mark - MKMapViewDelegate methods

- (MKAnnotationView *) mapView:(MKMapView *)aMapView viewForAnnotation:(id < MKAnnotation >)anAnnotation
{
    if ([anAnnotation isKindOfClass:[MKUserLocation class]])
    {
        ((MKUserLocation *)anAnnotation).title = TALocalizedString(@"Me");
        
        return nil;
    }
    else if ([anAnnotation isKindOfClass:[TAStreetAnnotation class]])
    {
        static NSString *reuseIdentifier = @"streetAnnotation";
        
        TAStreetAnnotation *_streetAnnotation = (TAStreetAnnotation *) anAnnotation;
        
        ZSPinAnnotation *_pinView = [[ZSPinAnnotation alloc] initWithAnnotation:_streetAnnotation reuseIdentifier:reuseIdentifier];
        _pinView.annotationType = ZSPinAnnotationTypeStandard;
        
        _pinView.canShowCallout = YES;
        _pinView.annotation = _streetAnnotation;
        _pinView.annotationColor = _streetAnnotation.color;
        
        return _pinView;
    }
    
    return nil;
}

- (void) mapView:(MKMapView *)theMapView didAddAnnotationViews:(NSArray *)theViews
{
    for (MKAnnotationView *_annotationView in theViews)
    {
        if ([_annotationView isKindOfClass:[ZSPinAnnotation class]])
        {
            [theMapView selectAnnotation:_annotationView.annotation animated:YES];
        }
    }
}

- (void) mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    cancelToolbarAnimation = YES;
}

- (void) mapView:(MKMapView *)theMapView didDeselectAnnotationView:(MKAnnotationView *)theView
{
    cancelToolbarAnimation = YES;
    
    if ([theView isKindOfClass:[ZSPinAnnotation class]])
    {
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            if (theView)
            {
                theView.alpha = 0;
            }
        } completion:^(BOOL finished) {
            if (theView && theView.annotation)
            {
                [mapView removeAnnotation:theView.annotation];
            }
        }];
    }
}

- (MKOverlayRenderer *) mapView:(MKMapView *)aMapView rendererForOverlay:(id<MKOverlay>)theOverlay
{
    MKPolyline *_polyline = (MKPolyline *) theOverlay;
    TAStreet *_street = [dataRetriever.streets objectForKey:_polyline.title];
    TAPolylineRenderer *renderer;
    
    @synchronized(allPolylineRenderers)
    {
        renderer = (TAPolylineRenderer *) [allPolylineRenderers objectForKey:_street.identifier];
    }
    
    if (!renderer)
    {
        NSArray *_lineDashPattern = [self lineDashPatternForStreet:_street];
        
        renderer = [[TAPolylineRenderer alloc] initWithPolyline:_polyline lineDashPattern:_lineDashPattern];
        renderer.lineWidth = 4.0;
        renderer.alpha = 0.9;
        renderer.strokeColor = [self colorForStreet:_street];
        renderer.speed = _street.speed / 3.6; // convert to m/s
        renderer.hideDashesWhenStopped = YES;
        
        @synchronized(allPolylineRenderers)
        {
            [allPolylineRenderers setObject:renderer forKey:_street.identifier];
        }
    }
    else
    {
        [renderer setNeedsDisplay];
    }
    
    if (!isMapMoving && currentZoomLevel >= MINIMUM_ANIMATION_ZOOM_LEVEL && [_polyline intersectsMapRect:aMapView.visibleMapRect])
    {
        [renderer startAnimating];
    }
    else
    {
        [renderer stopAnimating];
    }
    
    return renderer;
}

- (void) mapView:(MKMapView *)aMapView regionWillChangeAnimated:(BOOL)animated
{
    if ([aMapView isEqual:mapView])
    {
        isMapMoving = YES;
        
        [self showScale];
        
        [self stopPolylineAnimation];
        
        @synchronized(visiblePolylineRenderers)
        {
            [visiblePolylineRenderers removeAllObjects];
        }
    }
}

- (void)mapView:(MKMapView *)aMapView regionDidChangeAnimated:(BOOL)isAnimated
{
    if ([aMapView isEqual:mapView])
    {
        currentZoomLevel = aMapView.zoomLevel;
        
        isMapMoving = NO;
        
        [self hideScaleAfter:2.0];
        
        for (id<MKOverlay> _overlay in aMapView.overlays)
        {
            if ([_overlay isKindOfClass:[MKPolyline class]])
            {
                TAStreet *_street = [dataRetriever.streets objectForKey:_overlay.title];
                
                if (!_street)
                {
                    // This means that an update occurred meanwhile and that the streets have changed
                    break;
                }
                
                if ([_overlay intersectsMapRect:aMapView.visibleMapRect])
                {
                    TAPolylineRenderer *_renderer = (TAPolylineRenderer *) [aMapView rendererForOverlay:_overlay];
                    
                    @synchronized(visiblePolylineRenderers)
                    {
                        [visiblePolylineRenderers setObject:_renderer forKey:_street.identifier];
                    }
                }
            }
        }
        
        if (mapView.camera.pitch == 0 && currentZoomLevel > MINIMUM_ANIMATION_ZOOM_LEVEL)
        {
            [self beginPolylineAnimationAfter:0.1];
        }
        else
        {
            [self stopPolylineAnimation];
        }
    }
    
    if (didLoadMap && mapView.userTrackingMode == MKUserTrackingModeNone)
    {
        NSUserDefaults *_defaults = [NSUserDefaults standardUserDefaults];
        [_defaults setDouble:mapView.camera.centerCoordinate.latitude forKey:INITIAL_MAP_CAMERA_LATITUDE];
        [_defaults setDouble:mapView.camera.centerCoordinate.longitude forKey:INITIAL_MAP_CAMERA_LONGITUDE];
        [_defaults setDouble:mapView.camera.heading forKey:INITIAL_MAP_CAMERA_HEADING];
        [_defaults setDouble:mapView.camera.altitude forKey:INITIAL_MAP_CAMERA_ALTITUDE];
    }
}

- (void)mapView:(MKMapView *)theMapView didChangeUserTrackingMode:(MKUserTrackingMode)theMode animated:(BOOL)isAnimated
{
    if (theMode == MKUserTrackingModeNone)
    {
        theMapView.scrollEnabled = YES;
        locationButton.selected = NO;
    }
    else
    {
        NSString *_iconImageName = (theMode == MKUserTrackingModeFollow) ? @"location-icon-selected" : @"location-icon-following";
        [locationButton setImage:[UIImage imageNamed:_iconImageName] forState:UIControlStateSelected];
        
        theMapView.scrollEnabled = NO;
        locationButton.selected = YES;
    }
    
    [[NSUserDefaults standardUserDefaults] setInteger:theMode forKey:INITIAL_MAP_MODE_KEY];
}

- (void)mapViewDidFinishLoadingMap:(MKMapView *)theMapView
{
    didLoadMap = YES;
    
    // Restore tracking mode state
    MKUserTrackingMode _mapTrackingMode = [[NSUserDefaults standardUserDefaults] integerForKey:INITIAL_MAP_MODE_KEY];
    [theMapView setUserTrackingMode:_mapTrackingMode animated:YES];
}

- (void)mapViewWillStartLocatingUser:(MKMapView *)theMapView
{
    self.loadingContainer.hidden = NO;
}

- (void)mapView:(MKMapView *)theMapView didUpdateUserLocation:(MKUserLocation *)theUserLocation
{
    if (theUserLocation.location && !dataRetriever.updateInProgress)
    {
        self.loadingContainer.hidden = YES;
    }
}

- (void)mapView:(MKMapView *)theMapView didFailToLocateUserWithError:(NSError *)theError
{
    if (!dataRetriever.updateInProgress)
    {
        self.loadingContainer.hidden = YES;
    }
    
    if (self.isVisble)
    {
        // Revert tracking mode to MKUserTrckingModeNone
        theMapView.userTrackingMode = MKUserTrackingModeNone;
        
        [[UIApplication sharedApplication] dismissAllAlertViews];
        
        UIAlertView *_alert = [[UIAlertView alloc] initWithTitle:TALocalizedString(@"Could not locate you")
                                                         message:nil
                                                        delegate:self
                                               cancelButtonTitle:TALocalizedString(@"Cancel")
                                               otherButtonTitles:TALocalizedString(@"Try again"), nil];
        
        _alert.tag = LOCATION_FAILED_ALERT_TAG;
    }
}

//------------------------------------------
// UIAlertViewDelegate methods
//------------------------------------------
#pragma mark - UIAlertViewDelegate methods

- (void) alertView:(UIAlertView *)theAlertView didDismissWithButtonIndex:(NSInteger)theButtonIndex
{
    if (theAlertView.tag == LOCATION_FAILED_ALERT_TAG)
    {
        if (theButtonIndex == 1)
        {
            [self locationAction:self];
        }
    }
}


//------------------------------------------
// UISearchBarDelegate methods
//------------------------------------------
#pragma mark - UISearchBarDelegate methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar
{
    searchBar.showsCancelButton = YES;
    
    UIButton *cancelButton;
    
    if ([localeManager.locale isEqualToString:LOCALE_ARABIC] && IS_IOS_7_OR_NEWER)
    {
        UIView *_topView = self.searchBar.subviews[0];
        
        for (UIView *subView in _topView.subviews)
        {
            if ([subView isKindOfClass:NSClassFromString(@"UINavigationButton")])
            {
                cancelButton = (UIButton*)subView;
            }
        }
        
        if (cancelButton)
        {
            [cancelButton setTitle:TALocalizedString(@"Cancel") forState:UIControlStateNormal];
        }
    }
    
    searchBar.width = self.view.width - 20;
    
    [self clearBarButtonItems];
    
    [self setSearchVisible:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)theSearchBar
{
    searchBar.showsCancelButton = NO;
    searchBar.width = [self searchBarWidth];
    
    [self initBarButtonItems];
    
    [self setSearchVisible:NO];
}

- (void) searchBarCancelButtonClicked:(UISearchBar *)theSearchBar
{
    searchBar.text = nil;
    
    [searchBar resignFirstResponder];
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self setSearchVisible:YES];
}

//------------------------------------------
// UIActionSheetDelegate methods
//------------------------------------------
#pragma mark - UIActionSheetDelegate methods

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == ACTION_SHEET_TAG)
    {
        if (buttonIndex == 0)
        {
            [self chooseScreenshotSize];
        }
        else if (buttonIndex == 1)
        {
            [self shareLocation];
        }
    }
    else
    {
        if (buttonIndex == 0)
        {
            [self shareSquareScreenshot];
        }
        else if (buttonIndex == 1)
        {
            [self shareFullScreenshot];
        }
    }
}

@end
