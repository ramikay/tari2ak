//
//  TATextViewController.m
//  Tari2ak
//
//  Created by Rami Khawandi on 1/1/14.
//  Copyright (c) 2014 Rami Khawandi. All rights reserved.
//

#import "TATextViewController.h"

@implementation TATextViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)isAnimated
{
    [super viewWillAppear:isAnimated];
    
    self.textView.text = self.text;
}

@end
