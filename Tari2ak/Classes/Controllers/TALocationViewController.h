//
//  TALocationViewController.h
//  Tari2ak
//
//  Created by Rami Khawandi on 5/12/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface TALocationViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@end
