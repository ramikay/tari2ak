//
//  TADataReporter.h
//  Tari2ak
//
//  Created by Rami on 7/5/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAReport.h"

@class AFHTTPRequestOperation;

@interface TADataReporter : NSObject

+ (void) sendReport:(TAReport *)theReport
            success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))aSuccessBlock
            failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))aFailureBlock;

@end
