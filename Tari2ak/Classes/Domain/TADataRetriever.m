//
//  TADataRetriever.m
//  Tari2ak
//
//  Created by Rami on 24/5/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import "TADataRetriever.h"
#import "AFNetworking.h"
#import "TAConstants.h"
#import "TAStreet.h"
#import <CoreLocation/CLLocation.h>
#import "NSString+AESCrypt.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "CLLocation+Utils.h"
#import "TAStreet+Geography.h"
#import "TACountry.h"
#import "TAArea.h"
#import "AFNetworkActivityIndicatorManager.h"

@interface TADataRetriever ()
{
    NSDate *lastUpdate;
    BOOL updateInProgress;
    
    NSDictionary *streets;
    
    double latitudeOffset;
    double longitudeOffset;
    
    NSTimer *autoRefreshTimer;
    
    dispatch_queue_t parseQueue;
}

- (void) autoRefreshIfNeeded;
- (void) parseResponse:(NSData *)theResponseData;

@end

//------------------------------------------
// Notifications
//------------------------------------------
#pragma mark - Notifications

NSString * const TADataRetrieverUpdateBegan                 = @"TADataRetrieverUpdateBegan";
NSString * const TADataRetrieverStreetsUpdateComplete       = @"TADataRetrieverStreetsUpdateComplete";
NSString * const TADataRetrieverUpdateFailed                = @"TADataRetrieverUpdateFailed";

@implementation TADataRetriever

//------------------------------------------
// Singleton methods
//------------------------------------------
#pragma mark - Singleton methods

+ (TADataRetriever *) sharedInstance
{
    static dispatch_once_t pred;
    static TADataRetriever *_shared = nil;
    
    dispatch_once(&pred, ^{
        _shared = [[TADataRetriever alloc] init];
    });
    
    return _shared;
}

//------------------------------------------
// Init
//------------------------------------------
#pragma mark - Init

- (id) init
{
    self = [super init];
    
    if (self != nil)
    {
        updateInProgress = NO;
        
        parseQueue = dispatch_queue_create("com.ramikay.tari2ak.parse.queue", DISPATCH_QUEUE_CONCURRENT);
    }
    
    return self;
}

//------------------------------------------
// Public methods
//------------------------------------------
#pragma mark - Public methods

- (void) update
{
    if (!updateInProgress)
    {
        updateInProgress = YES;
        
        NSNotificationCenter *_nc = [NSNotificationCenter defaultCenter];
        [_nc postNotificationName:TADataRetrieverUpdateBegan object:nil];
        
        AFNetworkActivityIndicatorManager *_spinny = [AFNetworkActivityIndicatorManager sharedManager];
        [_spinny incrementActivityCount];
        
        NSInteger _currentTimestamp = floor([[NSDate date] timeIntervalSince1970]);
        NSString *_tokenString = [NSString stringWithFormat:@"%d", _currentTimestamp];
        NSString *_encryptedToken = [_tokenString AES128EncryptWithKey:RETRIEVAL_KEY];
        
        NSDictionary *_params = [NSDictionary dictionaryWithObjects:@[_encryptedToken,@"apple"] forKeys:@[@"token",@"map"]];
        
        id tracker = [[GAI sharedInstance] defaultTracker];
        NSDate *startTime = [NSDate date];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        NSString *urlString = [NSString stringWithFormat:@"%@/v1.1/update", SERVER_URL];
        
        [manager GET:urlString parameters:_params success:^(AFHTTPRequestOperation *operation, id responseObject)
        {
             [_spinny decrementActivityCount];
             [self parseResponse:responseObject];
             
             // Analytics User Timing
             NSTimeInterval _responseTime = [[NSDate date] timeIntervalSinceDate:startTime];
             
             NSInteger _durationInteger = round(_responseTime*1000);
             NSNumber *_duration = [NSNumber numberWithDouble:_durationInteger];    // In milliseconds
             
             [tracker send:[[GAIDictionaryBuilder createTimingWithCategory:@"Data"
                                                                 interval:_duration
                                                                     name:@"Refresh"
                                                                    label:@"Success"] build]];
         }
                    failure:^(AFHTTPRequestOperation *theOperation, NSError *theError)
         {
             updateInProgress = NO;
             [_spinny decrementActivityCount];
             [_nc postNotificationName:TADataRetrieverUpdateFailed object:theError];
             
             // Analytics User Timing
             NSTimeInterval _responseTime = [[NSDate date] timeIntervalSinceDate:startTime];
             
             NSInteger _durationInteger = round(_responseTime*1000);
             NSNumber *_duration = [NSNumber numberWithDouble:_durationInteger];    // In milliseconds
             
             NSString *_errorString = [NSString stringWithFormat:@"Error: %ld", (long)theError.code];
             
             [tracker send:[[GAIDictionaryBuilder createTimingWithCategory:@"Data"
                                                                  interval:_duration
                                                                      name:@"Refresh"
                                                                     label:_errorString] build]];
         }];
    }
}

- (void) startAutoRefresh
{
    [self autoRefreshIfNeeded];
    
    autoRefreshTimer = [NSTimer scheduledTimerWithTimeInterval:UPDATE_CYCLE target:self selector:@selector(autoRefreshIfNeeded) userInfo:nil repeats:YES];
}

- (void) stopAutoRefresh
{
    [autoRefreshTimer invalidate];
}

- (BOOL) updateInProgress
{
    return updateInProgress;
}

- (NSDate *) lastUpdate
{
    return lastUpdate;
}

- (NSDictionary *) streets
{
    __block id object;
    
    dispatch_sync(parseQueue, ^{
        object = streets;
    });
    
    return (NSDictionary *) object;
}

- (CLLocationDistance) latitudeOffset
{
    return latitudeOffset;
}

- (CLLocationDistance) longitudeOffset
{
    return longitudeOffset;
}

//------------------------------------------
// Private methods
//------------------------------------------
#pragma mark - Private methods

- (void) autoRefreshIfNeeded
{
    if (lastUpdate == nil)
    {
        [self update];
    }
    else
    {
        NSDate *_now = [NSDate date];
        NSTimeInterval _updatedAgo = [_now timeIntervalSinceDate:lastUpdate];
        
        if (_updatedAgo >= UPDATE_CYCLE)
        {
            [self update];
        }
    }
}

- (void) parseResponse:(NSDictionary *)reponse
{
    dispatch_barrier_async(parseQueue, ^{
        
#ifdef DEBUG
        NSLog(@"%@", reponse);
#endif
        NSDictionary *_meta = [reponse objectForKey:@"meta"];
        
        if (_meta)
        {
            latitudeOffset = [[_meta objectForKey:@"latitudeOffset"] doubleValue];
            longitudeOffset = [[_meta objectForKey:@"longitudeOffset"] doubleValue];
        }
        
        NSArray *_results = [reponse objectForKey:@"data"];
        
        NSMutableDictionary *_streets = [[NSMutableDictionary alloc] initWithCapacity:_results.count];
        
        for (NSDictionary *_result in _results)
        {
            NSString *_id =  [_result objectForKey:@"id"];
            
            float _speed =  [[_result objectForKey:@"speed"] floatValue];
            
            int _timestamp = [[_result objectForKey:@"last_updated"] intValue];
            NSDate *_modifyDate = [NSDate dateWithTimeIntervalSince1970:_timestamp];

            NSDictionary *_names = [_result objectForKey:@"name"];
            
            NSArray *_nodes = [[_result objectForKey:@"polyline"] componentsSeparatedByString:@","];
            NSMutableArray *_polyline = [[NSMutableArray alloc] initWithCapacity:_nodes.count];
            
            NSString *_type = [_result objectForKey:@"type"];
            TATrafficCondition _condition = [[_result objectForKey:@"condition"] intValue];
            
            CLLocation *_midPoint = nil;
            NSInteger _nodeCount = _nodes.count;
            
            for (int _i = 0; _i < _nodeCount; _i++)
            {
                NSString *_node = [_nodes objectAtIndex:_i];
                NSArray *_degrees = [_node componentsSeparatedByString:@" "];

                float _lat = [[_degrees objectAtIndex:0] floatValue];
                float _lon = [[_degrees objectAtIndex:1] floatValue];
                
                CLLocation *_point = [[CLLocation alloc] initWithLatitude:_lat longitude:_lon];
                
                if (longitudeOffset || latitudeOffset)
                {
                    _point = [_point locationByOffsettingLongitude:longitudeOffset latitude:latitudeOffset];
                }
                
                // Get the middle node
                if (_i == floor(_nodeCount / 2))
                {
                    _midPoint = _point;
                }
                
                [_polyline addObject:_point];
            }
            
            TAStreet *_street = [[TAStreet alloc] initWithIdentifier:_id
                                                               names:_names
                                                               speed:_speed
                                                               nodes:_polyline
                                                            midPoint:_midPoint
                                                                type:_type
                                                           condition:_condition
                                                        modifiedDate:_modifyDate];
            
            [_streets setObject:_street forKey:_id];
        }
        
        updateInProgress = NO;
        
        streets = [NSDictionary dictionaryWithDictionary:_streets];
        
        lastUpdate = [NSDate date];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:TADataRetrieverStreetsUpdateComplete object:streets];
        });
    });
}

@end
