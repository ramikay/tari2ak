//
//  TADataReporter.m
//  Tari2ak
//
//  Created by Rami on 7/5/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import "TADataReporter.h"
#import "AFNetworking.h"
#import "TAConstants.h"
#import "NSString+AESCrypt.h"
#import "TAAppDelegate.h"
#import "NSString+Utils.h"

@implementation TADataReporter

//------------------------------------------
// Class methods
//------------------------------------------
#pragma mark - Class methods

+ (void) sendReport:(TAReport *)theReport
            success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))aSuccessBlock
            failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))aFailureBlock
{
    NSString *urlString = [NSString stringWithFormat:@"%@/report", SERVER_URL];
    UIDevice *_currentDevice = [UIDevice currentDevice];
    CLLocationCoordinate2D _coordinate = theReport.coordinate;
    
    TALocaleManager *_lm = [TALocaleManager sharedInstance];
    
    NSString *_reportParams = [NSString stringWithFormat:@"%@|%f,%f|%f|%f|%d|%@|%f",
                               _currentDevice.identifierForVendor.UUIDString,
                               _coordinate.latitude,
                               _coordinate.longitude,
                               theReport.speed,
                               [theReport.timestamp timeIntervalSince1970],
                               theReport.isInBackground,
                               _lm.locale,
                               theReport.northAngle];
    
    NSString *_encryptedString = [_reportParams AES128EncryptWithKey:REPORTING_KEY];
    NSDictionary *_params = [NSDictionary dictionaryWithObject:_encryptedString forKey:@"report"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:urlString parameters:_params success:aSuccessBlock failure:aFailureBlock];
}

@end
