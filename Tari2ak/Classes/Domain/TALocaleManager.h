//
//  TALocaleManager.h
//  Tari2ak
//
//  Created by Rami Khawandi on 22/5/14.
//  Copyright (c) 2014 Rami Khawandi. All rights reserved.
//

#import <Foundation/Foundation.h>

// Fired when the value of the locale property changes.
// This NSNotification carries an NSString object representing the locale key
extern NSString * const TALocaleChanged;

@interface TALocaleManager : NSObject

+ (TALocaleManager *) sharedInstance;

// Setting the value of locale fires up the TALocaleChanged notification
@property (strong, nonatomic) NSString *locale;
@property (readonly, nonatomic) NSBundle *localeBundle;
@property (readonly, nonatomic) NSString *languageID;

+ (NSBundle *) localeBundle;

+ (NSArray *) supportedLocales; // An app-wide constant containing supported localizations

@end
