//
//  TADataRetriever.h
//  Tari2ak
//
//  Created by Rami on 24/5/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CLLocation.h>

// Fired when an update begins. This notification carries no object.
extern NSString * const TADataRetrieverUpdateBegan;

// Fired when the streets are updated successfully.
// This notification's object is a NSDictionary of TAStreet objects,
// which can be retrived via the streets property.
extern NSString * const TADataRetrieverStreetsUpdateComplete;

// Fired when an update fails. This notification carries a NSError object.
extern NSString * const TADataRetrieverUpdateFailed;

/* A singleton object responsible for carrying out API requests, parsing results,
 * sorting them in memory, and notifying interested objects. */

@interface TADataRetriever : NSObject

+ (TADataRetriever *) sharedInstance;

- (void) update;

- (void) startAutoRefresh;
- (void) stopAutoRefresh;

@property (nonatomic, readonly) BOOL updateInProgress;
@property (nonatomic, readonly) BOOL geocodingInProgress;

@property (nonatomic, readonly) NSDate *lastUpdate;

// Key is Unique Street ID, value is TAStreet object
@property (nonatomic, readonly) NSDictionary *streets;

// Key is ISO country code, value is a TACountry object
@property (nonatomic, readonly) NSDictionary *countries;

@property (nonatomic, readonly) CLLocationDistance latitudeOffset;

@property (nonatomic, readonly) CLLocationDistance longitudeOffset;

@end
