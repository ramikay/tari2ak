//
//  TALocaleManager.m
//  Tari2ak
//
//  Created by Rami Khawandi on 22/5/14.
//  Copyright (c) 2014 Rami Khawandi. All rights reserved.
//

#import "TALocaleManager.h"
#import "TAConstants.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import "TATelephony.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"

//------------------------------------------
#pragma mark - Constants
//------------------------------------------

NSString * const TALocaleChanged = @"TALocaleChanged";

//------------------------------------------
#pragma mark - Private interface
//------------------------------------------

@interface TALocaleManager ()
{
    NSString *languageID;
    NSBundle *localeBundle;
}

@end

//------------------------------------------
#pragma mark - Implementation
//------------------------------------------

@implementation TALocaleManager

@synthesize locale;

//------------------------------------------
#pragma mark - Singleton method
//------------------------------------------

+ (TALocaleManager *) sharedInstance
{
    static dispatch_once_t pred;
    static TALocaleManager *_shared = nil;
    
    dispatch_once(&pred, ^{
        _shared = [[TALocaleManager alloc] init];
    });
    
    return _shared;
}

//------------------------------------------
#pragma mark - Init
//------------------------------------------

- (id) init
{
    self = [super init];
    
    if (self)
    {
        NSString *_systemLocale = [[NSLocale preferredLanguages] objectAtIndex:0];
        NSString *_locale = [[NSUserDefaults standardUserDefaults] valueForKey:INITIAL_LOCALE_KEY];
        
        // Check if it's a supported locale
        BOOL _supportedLocale = (_locale != nil && [TALocaleManager.supportedLocales containsObject:_locale]);
        
        if (!_supportedLocale)
        {
            // Make sure CoreTelephony exists in order not to alieniate iPad and iPod touch users.
            if ([CTTelephonyNetworkInfo class])
            {
                if ([[TATelephony isoCountryCode].lowercaseString isEqualToString:@"lb"])
                {
                    self.locale = LOCALE_ARABIC;
                }
                else
                {
                    self.locale = LOCALE_ENGLISH;
                }
            }
            else if ([TALocaleManager.supportedLocales containsObject:_systemLocale])
            {
                self.locale = _systemLocale;
            }
            else
            {
                self.locale = LOCALE_ENGLISH;
            }
        }
        else
        {
            self.locale = _locale;
        }
    }
    
    return self;
}

//------------------------------------------
#pragma mark - Public methods
//------------------------------------------

- (void) setLocale:(NSString *)aLocale
{
    if (aLocale)
    {
        if ([TALocaleManager.supportedLocales containsObject:aLocale])
        {
            locale = aLocale;
            languageID = [[locale componentsSeparatedByString:@"-"] objectAtIndex:0];
            localeBundle = nil;
            
            [[NSUserDefaults standardUserDefaults] setObject:aLocale forKey:INITIAL_LOCALE_KEY];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:TALocaleChanged object:self.locale];
        }
    }
}

- (NSString *) languageID
{
    return languageID;
}

- (NSBundle *) localeBundle
{
    if (!localeBundle)
    {
        NSString *_chosenLocale = locale;
        
        if ([locale  isEqual:LOCALE_ARABIC] && [CTTelephonyNetworkInfo class])
        {
            if ([[TATelephony isoCountryCode].lowercaseString isEqualToString:@"lb"])
            {
                _chosenLocale = LOCALE_ARABIC_LEBANON;
            }
        }
        
        NSString *_bundlePath = [[NSBundle mainBundle] pathForResource:@"Localizable" ofType:@"strings" inDirectory:nil forLocalization:_chosenLocale];
        
        localeBundle = [[NSBundle alloc] initWithPath:[_bundlePath stringByDeletingLastPathComponent]];
    }
    
    return localeBundle;
}

+ (NSBundle *) localeBundle
{
    return [TALocaleManager sharedInstance].localeBundle;
}

+ (NSArray *) supportedLocales
{
    return @[LOCALE_ENGLISH, LOCALE_FRENCH, LOCALE_ARABIC, LOCALE_ARMENIAN];
}

@end
