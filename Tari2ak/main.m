//
//  main.m
//  Tari2ak
//
//  Created by Rami on 22/3/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TAAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TAAppDelegate class]));
    }
}
